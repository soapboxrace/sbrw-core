https://redis.io/docs/latest/develop/use/keyspace-notifications/

need to enable redis notifications, inside redis-cli type:

```
config set notify-keyspace-events KEA
```

```bash
docker exec -it redis redis-cli config set notify-keyspace-events KEA
```
