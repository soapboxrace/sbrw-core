package world.soapboxrace.sbrace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SbRaceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SbRaceApplication.class, args);
    }

}
