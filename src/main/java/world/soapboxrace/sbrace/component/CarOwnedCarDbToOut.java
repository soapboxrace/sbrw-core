package world.soapboxrace.sbrace.component;

import org.springframework.beans.BeanUtils;
import world.soapboxrace.sbrace.controller.out.*;
import world.soapboxrace.sbrace.data.entity.*;

import java.util.ArrayList;
import java.util.List;

public class CarOwnedCarDbToOut {

    public static OwnedCarTransOut copyOwnedCar(CarOwnedCar carOwnedCar) {
        OwnedCarTransOut ownedCarTransOut = new OwnedCarTransOut();
        if (carOwnedCar == null) {
            return null;
        }
        BeanUtils.copyProperties(carOwnedCar, ownedCarTransOut);
        ownedCarTransOut.setCustomCar(copyCustomCar(carOwnedCar.getCustomCar()));
        return ownedCarTransOut;
    }

    public static CustomCarOut copyCustomCar(CarCustomCar carCustomCar) {
        CustomCarOut customCarOut = new CustomCarOut();
        if (carCustomCar == null) {
            return null;
        }
        BeanUtils.copyProperties(carCustomCar, customCarOut);
        customCarOut.setPaints(copyPaints(carCustomCar.getPaints()));
        customCarOut.setPerformanceParts(copyPerformanceParts(carCustomCar.getPerformanceParts()));
        customCarOut.setSkillModParts(copySkillModParts(carCustomCar.getSkillModParts()));
        customCarOut.setVinyls(copyVinyls(carCustomCar.getVinyls()));
        customCarOut.setVisualParts(copyVisualParts(carCustomCar.getVisualParts()));
        return customCarOut;
    }

    public static List<PaintsOut> copyPaints(List<CarPaint> paintList) {
        List<PaintsOut> paintsOuts = new ArrayList<>();
        if (paintList == null) {
            return paintsOuts;
        }
        paintList.forEach(carPaint -> {
            PaintsOut paintsOut = new PaintsOut();
            CustomPaintTransOut customPaintTrans = new CustomPaintTransOut();
            BeanUtils.copyProperties(carPaint, customPaintTrans);
            paintsOut.setCustomPaintTrans(customPaintTrans);
            paintsOuts.add(paintsOut);
        });
        return paintsOuts;
    }

    public static List<PerformancePartsOut> copyPerformanceParts(List<CarPerformancePart> performancePartList) {
        List<PerformancePartsOut> performancePartsOuts = new ArrayList<>();
        if (performancePartList == null) {
            return performancePartsOuts;
        }
        performancePartList.forEach(carPerformancePart -> {
            PerformancePartsOut performancePartsOut = new PerformancePartsOut();
            PerformancePartTransOut performancePartTrans = new PerformancePartTransOut();
            BeanUtils.copyProperties(carPerformancePart, performancePartTrans);
            performancePartsOut.setPerformancePartTrans(performancePartTrans);
            performancePartsOuts.add(performancePartsOut);
        });
        return performancePartsOuts;
    }

    public static List<SkillModPartsOut> copySkillModParts(List<CarSkillModPart> skillModPartList) {
        List<SkillModPartsOut> skillModPartsOuts = new ArrayList<>();
        if (skillModPartList == null) {
            return skillModPartsOuts;
        }
        skillModPartList.forEach(carSkillModPart -> {
            SkillModPartsOut skillModPartsOut = new SkillModPartsOut();
            SkillModPartTransOut skillModPartTrans = new SkillModPartTransOut();
            BeanUtils.copyProperties(carSkillModPart, skillModPartTrans);
            skillModPartsOut.setSkillModPartTrans(skillModPartTrans);
            skillModPartsOuts.add(skillModPartsOut);
        });
        return skillModPartsOuts;
    }

    public static List<VinylsOut> copyVinyls(List<CarVinyl> vinylList) {
        List<VinylsOut> vinylsOuts = new ArrayList<>();
        if (vinylList == null) {
            return vinylsOuts;
        }
        vinylList.forEach(carVinyl -> {
            VinylsOut vinylsOut = new VinylsOut();
            CustomVinylTransOut customVinylTrans = new CustomVinylTransOut();
            BeanUtils.copyProperties(carVinyl, customVinylTrans);
            vinylsOut.setCustomVinylTrans(customVinylTrans);
            vinylsOuts.add(vinylsOut);
        });
        return vinylsOuts;
    }

    public static List<VisualPartsOut> copyVisualParts(List<CarVisualPart> visualPartList) {
        List<VisualPartsOut> visualPartsOuts = new ArrayList<>();
        if (visualPartList == null) {
            return visualPartsOuts;
        }
        visualPartList.forEach(carVisualPart -> {
            VisualPartsOut visualPartsOut = new VisualPartsOut();
            VisualPartTransOut visualPartTrans = new VisualPartTransOut();
            BeanUtils.copyProperties(carVisualPart, visualPartTrans);
            visualPartsOut.setVisualPartTrans(visualPartTrans);
            visualPartsOuts.add(visualPartsOut);
        });
        return visualPartsOuts;
    }

}
