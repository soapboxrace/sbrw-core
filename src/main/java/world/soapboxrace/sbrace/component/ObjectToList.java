package world.soapboxrace.sbrace.component;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

public class ObjectToList {

    public static <T> List<T> convertToList(Object objectIn, Class<T> toValueType) {
        List<T> tmpList = new ArrayList<>();
        if (objectIn instanceof List) {
            ((List<?>) objectIn).forEach(o -> {
                T tmpObj = convertObject(o, toValueType);
                tmpList.add(tmpObj);
            });
            return tmpList;
        } else {
            T tmpObj = convertObject(objectIn, toValueType);
            if (tmpObj != null) {
                tmpList.add(tmpObj);
            }
        }
        return tmpList;
    }

    public static <T> T convertObject(Object objectIn, Class<T> toValueType) {
        ObjectMapper objectMapper = ObjectMapperStatic.get();
        return objectMapper.convertValue(objectIn, toValueType);
    }

}
