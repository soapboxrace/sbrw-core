package world.soapboxrace.sbrace.component;

import org.springframework.beans.BeanUtils;
import world.soapboxrace.sbrace.controller.in.*;
import world.soapboxrace.sbrace.controller.out.*;
import world.soapboxrace.sbrace.data.entity.*;

import java.util.ArrayList;
import java.util.List;

public class OwnedCarInToDb {

    public static CarOwnedCar copyOwnedCar(OwnedCarTransIn ownedCarIn) {
        CarOwnedCar carOwnedCar = new CarOwnedCar();
        carOwnedCar.setCustomCar(copyCustomCar(ownedCarIn.getCustomCar()));
        BeanUtils.copyProperties(ownedCarIn, carOwnedCar);
        return carOwnedCar;
    }

    static CarCustomCar copyCustomCar(CustomCarIn customCar) {
        if (customCar == null) {
            return null;
        }
        CarCustomCar carCustomCar = new CarCustomCar();
        BeanUtils.copyProperties(customCar, carCustomCar);

        carCustomCar.setPaints(copyPaints(customCar.getPaints()));
        carCustomCar.setPerformanceParts(copyPerformanceParts(customCar.getPerformanceParts()));
        carCustomCar.setSkillModParts(copySkillModParts(customCar.getSkillModParts()));
        carCustomCar.setVinyls(copyVinyls(customCar.getVinyls()));
        carCustomCar.setVisualParts(copyVisualParts(customCar.getVisualParts()));

        return carCustomCar;
    }

    static List<CarPaint> copyPaints(PaintsIn paints) {
        if (paints == null) {
            return null;
        }
        List<CustomPaintTransOut> customPaintTrans = paints.getCustomPaintTrans();
        ArrayList<CarPaint> carPaints = new ArrayList<>();
        customPaintTrans.forEach(paintsOut -> {
            CarPaint carPaint = new CarPaint();
            BeanUtils.copyProperties(paintsOut, carPaint);
            carPaints.add(carPaint);
        });
        return carPaints;
    }

    static List<CarPerformancePart> copyPerformanceParts(PerformancePartsIn performanceParts) {
        if (performanceParts == null) {
            return null;
        }
        ArrayList<CarPerformancePart> carPerformanceParts = new ArrayList<>();
        List<PerformancePartTransOut> performancePartTrans = performanceParts.getPerformancePartTrans();
        performancePartTrans.forEach(performancePartsOut -> {
            CarPerformancePart carPerformancePart = new CarPerformancePart();
            BeanUtils.copyProperties(performancePartsOut, carPerformancePart);
            carPerformanceParts.add(carPerformancePart);
        });
        return carPerformanceParts;
    }

    static List<CarSkillModPart> copySkillModParts(SkillModPartsIn skillModParts) {
        if (skillModParts == null) {
            return null;
        }
        List<SkillModPartTransOut> skillModPartTrans = skillModParts.getSkillModPartTrans();
        ArrayList<CarSkillModPart> carSkillModParts = new ArrayList<>();
        skillModPartTrans.forEach(skillModPartsOut -> {
            CarSkillModPart carSkillModPart = new CarSkillModPart();
            BeanUtils.copyProperties(skillModPartsOut, carSkillModPart);
            carSkillModParts.add(carSkillModPart);
        });
        return carSkillModParts;
    }

    static List<CarVinyl> copyVinyls(VinylsIn vinyls) {
        if (vinyls == null) {
            return null;
        }
        ArrayList<CarVinyl> carVinyls = new ArrayList<>();
        List<CustomVinylTransOut> customVinylTrans = vinyls.getCustomVinylTrans();
        customVinylTrans.forEach(vinylsOut -> {
            CarVinyl carVinyl = new CarVinyl();
            BeanUtils.copyProperties(vinylsOut, carVinyl);
            carVinyls.add(carVinyl);
        });
        return carVinyls;
    }

    static List<CarVisualPart> copyVisualParts(VisualPartsIn visualParts) {
        if (visualParts == null) {
            return null;
        }
        ArrayList<CarVisualPart> carVisualParts = new ArrayList<>();
        List<VisualPartTransOut> visualPartTrans = visualParts.getVisualPartTrans();
        visualPartTrans.forEach(visualPartsOut -> {
            CarVisualPart carVisualPart = new CarVisualPart();
            BeanUtils.copyProperties(visualPartsOut, carVisualPart);
            carVisualParts.add(carVisualPart);
        });
        return carVisualParts;
    }

}
