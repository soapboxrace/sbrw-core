package world.soapboxrace.sbrace.component;

import org.springframework.beans.BeanUtils;
import world.soapboxrace.sbrace.controller.out.*;
import world.soapboxrace.sbrace.controller.wrapper.PersonasDefaultcarWrapper;
import world.soapboxrace.sbrace.data.entity.*;

import java.util.ArrayList;
import java.util.List;

public class OwnedCarOutToDb {

    public static CarOwnedCar copyOwnedCar(PersonasDefaultcarWrapper ownedCarWrapper) {
        CarOwnedCar carOwnedCar = new CarOwnedCar();
        OwnedCarTransOut ownedCarTrans = ownedCarWrapper.getOwnedCarTrans();
        carOwnedCar.setCustomCar(copyCustomCar(ownedCarTrans.getCustomCar()));
        BeanUtils.copyProperties(ownedCarTrans, carOwnedCar);
        return carOwnedCar;
    }

    static CarCustomCar copyCustomCar(CustomCarOut customCar) {
        if (customCar == null) {
            return null;
        }
        CarCustomCar carCustomCar = new CarCustomCar();
        BeanUtils.copyProperties(customCar, carCustomCar);
        carCustomCar.setName((String) customCar.getName());

        carCustomCar.setPaints(copyPaints(customCar.getPaints()));
        carCustomCar.setPerformanceParts(copyPerformanceParts(customCar.getPerformanceParts()));
        carCustomCar.setSkillModParts(copySkillModParts(customCar.getSkillModParts()));
        carCustomCar.setVinyls(copyVinyls(customCar.getVinyls()));
        carCustomCar.setVisualParts(copyVisualParts(customCar.getVisualParts()));

        return carCustomCar;
    }

    static List<CarPaint> copyPaints(List<PaintsOut> paints) {
        if (paints == null) {
            return null;
        }
        ArrayList<CarPaint> carPaints = new ArrayList<>();
        paints.forEach(paintsOut -> {
            CustomPaintTransOut customPaintTrans = paintsOut.getCustomPaintTrans();
            CarPaint carPaint = new CarPaint();
            BeanUtils.copyProperties(customPaintTrans, carPaint);
            carPaints.add(carPaint);
        });
        return carPaints;
    }

    static List<CarPerformancePart> copyPerformanceParts(List<PerformancePartsOut> performanceParts) {
        if (performanceParts == null) {
            return null;
        }
        ArrayList<CarPerformancePart> carPerformanceParts = new ArrayList<>();
        performanceParts.forEach(performancePartsOut -> {
            PerformancePartTransOut performancePartTrans = performancePartsOut.getPerformancePartTrans();
            CarPerformancePart carPerformancePart = new CarPerformancePart();
            BeanUtils.copyProperties(performancePartTrans, carPerformancePart);
            carPerformanceParts.add(carPerformancePart);
        });
        return carPerformanceParts;
    }

    static List<CarSkillModPart> copySkillModParts(List<SkillModPartsOut> skillModParts) {
        if (skillModParts == null) {
            return null;
        }
        ArrayList<CarSkillModPart> carSkillModParts = new ArrayList<>();
        skillModParts.forEach(skillModPartsOut -> {
            SkillModPartTransOut skillModPartTrans = skillModPartsOut.getSkillModPartTrans();
            CarSkillModPart carSkillModPart = new CarSkillModPart();
            BeanUtils.copyProperties(skillModPartTrans, carSkillModPart);
            carSkillModParts.add(carSkillModPart);
        });
        return carSkillModParts;
    }

    static List<CarVinyl> copyVinyls(List<VinylsOut> vinyls) {
        if (vinyls == null) {
            return null;
        }
        ArrayList<CarVinyl> carVinyls = new ArrayList<>();
        vinyls.forEach(vinylsOut -> {
            CustomVinylTransOut customVinylTrans = vinylsOut.getCustomVinylTrans();
            CarVinyl carVinyl = new CarVinyl();
            BeanUtils.copyProperties(customVinylTrans, carVinyl);
            carVinyls.add(carVinyl);
        });
        return carVinyls;
    }

    static List<CarVisualPart> copyVisualParts(List<VisualPartsOut> visualParts) {
        if (visualParts == null) {
            return null;
        }
        ArrayList<CarVisualPart> carVisualParts = new ArrayList<>();
        visualParts.forEach(visualPartsOut -> {
            VisualPartTransOut visualPartTrans = visualPartsOut.getVisualPartTrans();
            CarVisualPart carVisualPart = new CarVisualPart();
            BeanUtils.copyProperties(visualPartTrans, carVisualPart);
            carVisualParts.add(carVisualPart);
        });
        return carVisualParts;
    }

}
