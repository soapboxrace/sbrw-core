package world.soapboxrace.sbrace.config;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import world.soapboxrace.sbrace.session.TokenSessionService;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class HttpHeaderConfig implements Filter {

    private final TokenSessionService tokenSessionService;

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String securityToken = request.getHeader("securityToken");
        if (securityToken != null) {
            tokenSessionService.keepAlive(securityToken);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

}
