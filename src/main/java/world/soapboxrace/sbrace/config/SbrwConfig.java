package world.soapboxrace.sbrace.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("sbrw")
@Component
@Getter
@Setter
public class SbrwConfig {

    private String xmppFqdn;
    private Integer xmppPort;
    private String sbXmppCliUrl;
    private String udpRaceIp;
    private Integer udpRacePort;
    private String udpFreeroamIp;
    private Integer udpFreeroamPort;

}
