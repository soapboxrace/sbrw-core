package world.soapboxrace.sbrace.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.controller.out.AchievementsPacketOut;
import world.soapboxrace.sbrace.controller.wrapper.AchievementsLoadAllWrapper;
import world.soapboxrace.sbrace.service.PersonaService;

@RestController
@RequestMapping("/achievements")
@RequiredArgsConstructor
public class Achievements {

    private final PersonaService personaService;

    @GetMapping("/loadall")
    public AchievementsLoadAllWrapper loadall(@RequestHeader("securityToken") String securityToken,
                                              @RequestHeader("userId") Long userId) {
        Long activePersonaId = personaService.getActivePersonaId(securityToken);
        AchievementsLoadAllWrapper achievementsLoadAllWrapper = new AchievementsLoadAllWrapper();
        AchievementsPacketOut achievementsPacketOut = new AchievementsPacketOut();
        achievementsPacketOut.setPersonaId(activePersonaId);
        achievementsLoadAllWrapper.setAchievementsPacketOut(achievementsPacketOut);
        return achievementsLoadAllWrapper;
    }
}
