package world.soapboxrace.sbrace.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.controller.out.ArrayOfCarClassOut;
import world.soapboxrace.sbrace.controller.out.CarClassOut;
import world.soapboxrace.sbrace.controller.wrapper.CarClassesWrapper;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CarClasses {

    @GetMapping("/carclasses")
    public CarClassesWrapper carclasses() {
        CarClassesWrapper carClassesWrapper = new CarClassesWrapper();
        List<ArrayOfCarClassOut> arrayOfCarClass = new ArrayList<>();
        arrayOfCarClass.add(getCarClass(-2142411446, 999, 750));
        arrayOfCarClass.add(getCarClass(-405837480, 749, 600));
        arrayOfCarClass.add(getCarClass(-406473455, 599, 500));
        arrayOfCarClass.add(getCarClass(1866825865, 499, 400));
        arrayOfCarClass.add(getCarClass(415909161, 399, 250));
        arrayOfCarClass.add(getCarClass(872416321, 249, 0));
        carClassesWrapper.setArrayOfCarClass(arrayOfCarClass);
        return carClassesWrapper;
    }

    ArrayOfCarClassOut getCarClass(Integer carClassHash, Integer maxRating, Integer minRating) {
        ArrayOfCarClassOut arrayOfCarClassOut = new ArrayOfCarClassOut();
        CarClassOut carClass = new CarClassOut();
        carClass.setCarClassHash(carClassHash);
        carClass.setMaxRating(maxRating);
        carClass.setMinRating(minRating);
        arrayOfCarClassOut.setCarClass(carClass);
        return arrayOfCarClassOut;
    }

}
