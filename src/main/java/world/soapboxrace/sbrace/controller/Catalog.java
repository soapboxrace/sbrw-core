package world.soapboxrace.sbrace.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;
import world.soapboxrace.sbrace.controller.out.*;
import world.soapboxrace.sbrace.controller.wrapper.CatalogCategoriesWrapper;
import world.soapboxrace.sbrace.controller.wrapper.CatalogProductsInCategoryWrapper;
import world.soapboxrace.sbrace.data.entity.Product;
import world.soapboxrace.sbrace.data.entity.ProductCategory;
import world.soapboxrace.sbrace.service.ProductService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/catalog")
@RequiredArgsConstructor
public class Catalog {

    private final ProductService productService;

    @GetMapping("/productsInCategory")
    public CatalogProductsInCategoryWrapper productsInCategory(@RequestHeader("securityToken") String securityToken,
                                                               @RequestHeader("userId") String userId,
                                                               @RequestParam("userId") Long userIdParam,
                                                               @RequestParam("categoryName") String categoryName,
                                                               @RequestParam("clientProductType") String clientProductType,
                                                               @RequestParam("currencyType") String currencyType,
                                                               @RequestParam("language") String language) {
        CatalogProductsInCategoryWrapper catalogProductsInCategoryWrapper = new CatalogProductsInCategoryWrapper();
        catalogProductsInCategoryWrapper.setProductTrans(new ArrayList<>());
        List<Product> products = productService.getProductsByCategoryType(categoryName, clientProductType);
        products.forEach(product -> {
            ProductTransOutWrapper productTransOutWrapper = new ProductTransOutWrapper();
            ProductTransOut productTrans = new ProductTransOut();
            BeanUtils.copyProperties(product, productTrans);

            productTransOutWrapper.setProductTrans(productTrans);
            catalogProductsInCategoryWrapper.getProductTrans().add(productTransOutWrapper);
        });
        return catalogProductsInCategoryWrapper;
    }

    @GetMapping("/categories")
    public CatalogCategoriesWrapper categories() {
        List<ProductCategory> category = productService.getCategory();
        ArrayList<ArrayOfCategoryTransOut> arrayOfCategoryTrans = new ArrayList<>();
        category.forEach(productCategory -> {
            CategoryTransOut categoryTrans = new CategoryTransOut();
            BeanUtils.copyProperties(productCategory, categoryTrans);
            ArrayList<ProductsOut> products = new ArrayList<>();
            productCategory.getProductList().forEach(product -> {
                ProductsOut productsOut = new ProductsOut();
                ProductTransOut productTrans = new ProductTransOut();
                BeanUtils.copyProperties(product, productTrans);
                productsOut.setProductTrans(productTrans);
                products.add(productsOut);
                categoryTrans.setProducts(products);
            });
            ArrayOfCategoryTransOut arrayOfCategoryTransOut = new ArrayOfCategoryTransOut();

            arrayOfCategoryTransOut.setCategoryTrans(categoryTrans);
            arrayOfCategoryTrans.add(arrayOfCategoryTransOut);
        });
        CatalogCategoriesWrapper catalogCategoriesWrapper = new CatalogCategoriesWrapper();
        catalogCategoriesWrapper.setArrayOfCategoryTrans(arrayOfCategoryTrans);
        return catalogCategoriesWrapper;
    }

}
