package world.soapboxrace.sbrace.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import world.soapboxrace.sbrace.component.UUIDRandomBytes;
import world.soapboxrace.sbrace.controller.out.ClientServerCryptoTicketOut;
import world.soapboxrace.sbrace.controller.out.UdpRelayCryptoTicketOut;
import world.soapboxrace.sbrace.controller.wrapper.CryptoCryptoTicketWrapper;
import world.soapboxrace.sbrace.controller.wrapper.CryptoRelayCryptoTicketWrapper;
import world.soapboxrace.sbrace.service.BoostService;
import world.soapboxrace.sbrace.session.TokenSessionService;

import java.util.Base64;

@RestController
@RequestMapping("/crypto")
@RequiredArgsConstructor
public class Crypto {

    private final TokenSessionService tokenSessionService;
    private final UUIDRandomBytes uuidRandomBytes;
    private final BoostService boostService;

    @GetMapping("/cryptoticket")
    public CryptoCryptoTicketWrapper cryptoticket() {
        CryptoCryptoTicketWrapper cryptoCryptoTicketWrapper = new CryptoCryptoTicketWrapper();
        ClientServerCryptoTicketOut clientServerCryptoTicketOut = new ClientServerCryptoTicketOut();
        cryptoCryptoTicketWrapper.setClientServerCryptoTicketOut(clientServerCryptoTicketOut);
        if (boostService.waitingPayment(0L)) {
            return cryptoCryptoTicketWrapper;
        }
        clientServerCryptoTicketOut.setCryptoTicket("CgsMDQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=");
        clientServerCryptoTicketOut.setSessionKey("AAAAAAAAAAAAAAAAAAAAAA==");
        clientServerCryptoTicketOut.setTicketIv("AAAAAAAAAAAAAAAAAAAAAA==");
        return cryptoCryptoTicketWrapper;
    }

    @GetMapping("/relaycryptoticket/{personaId}")
    public CryptoRelayCryptoTicketWrapper relayCryptoTicket(@RequestHeader("securityToken") String securityToken,
                                                            @RequestHeader("userId") Long userId,
                                                            @PathVariable("personaId") Long personaId) {
        String activeRelayCryptoTicket = tokenSessionService.getActiveRelayCryptoTicket(securityToken);

        byte[] randomUUIDBytes = uuidRandomBytes.getRandomBytes();
        String ticketIV = Base64.getEncoder().encodeToString(randomUUIDBytes);

        CryptoRelayCryptoTicketWrapper cryptoRelayCryptoTicketWrapper = new CryptoRelayCryptoTicketWrapper();
        UdpRelayCryptoTicketOut udpRelayCryptoTicket = new UdpRelayCryptoTicketOut();
        udpRelayCryptoTicket.setCryptoTicket(activeRelayCryptoTicket);
        udpRelayCryptoTicket.setTicketIv(ticketIV);
        udpRelayCryptoTicket.setSessionKey("AAAAAAAAAAAAAAAAAAAAAA==");
        cryptoRelayCryptoTicketWrapper.setUdpRelayCryptoTicket(udpRelayCryptoTicket);
        return cryptoRelayCryptoTicketWrapper;
    }

}
