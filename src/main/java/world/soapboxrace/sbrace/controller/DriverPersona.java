package world.soapboxrace.sbrace.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;
import world.soapboxrace.sbrace.controller.out.*;
import world.soapboxrace.sbrace.controller.wrapper.*;
import world.soapboxrace.sbrace.data.entity.SbPersona;
import world.soapboxrace.sbrace.service.PersonaService;

import java.util.List;

@RestController
@RequestMapping("/DriverPersona")
@RequiredArgsConstructor
public class DriverPersona {

    private final PersonaService personaService;

    @GetMapping("/GetExpLevelPointsMap")
    public DriverPersonaGetExpLevelPointMapWrapper getExpLevelPointsMap() {
        DriverPersonaGetExpLevelPointMapWrapper driverPersonaGetExpLevelPointMapWrapper = new DriverPersonaGetExpLevelPointMapWrapper();
        driverPersonaGetExpLevelPointMapWrapper.setArrayOfInt(new ArrayOfIntOut());
        return driverPersonaGetExpLevelPointMapWrapper;
    }

    @PostMapping("/CreatePersona")
    public DriverPersonaCreatePersonaWrapper createPersona(
            @RequestHeader("securityToken") String securityToken,
            @RequestHeader("userId") Long userId,
            @RequestParam("name") String name,
            @RequestParam("iconIndex") Integer iconIndex) {
        SbPersona sbPersona = new SbPersona();
        sbPersona.setName(name);
        sbPersona.setIconIndex(iconIndex);
        sbPersona.setUserId(userId);
        SbPersona persona = personaService.createPersona(securityToken, sbPersona);
        DriverPersonaCreatePersonaWrapper driverPersonaCreatePersonaWrapper = new DriverPersonaCreatePersonaWrapper();
        ProfileDataOut profileData = new ProfileDataOut();
        BeanUtils.copyProperties(persona, profileData);
        profileData.setCash(sbPersona.getCash());
        driverPersonaCreatePersonaWrapper.setProfileData(profileData);
        return driverPersonaCreatePersonaWrapper;
    }

    @PostMapping("/UpdatePersonaPresence")
    public void UpdatePersonaPresence() {

    }

    @PostMapping("/GetPersonaBaseFromList")
    public DriverPersonaGetPersonaBaseFromListWrapper getPersonaBaseFromList(
            @RequestBody DriverPersonaGetPersonaBaseFromListWrapperIn personaIdArray) {
        List<Long> personaIdList = personaIdArray.getPersonaIdArray().getPersonaIds().getArrayLong();
        ArrayOfPersonaBaseOut arrayOfPersonaBase = new ArrayOfPersonaBaseOut();
        personaIdList.forEach(personaId -> {
            SbPersona sbPersona = personaService.getPersonaById(personaId);
            PersonaBaseOut personaBaseOut = new PersonaBaseOut();
            BeanUtils.copyProperties(sbPersona, personaBaseOut);
            personaBaseOut.setBadges("");
            personaBaseOut.setPresence(1);
            arrayOfPersonaBase.setPersonaBaseOut(personaBaseOut);
        });
        DriverPersonaGetPersonaBaseFromListWrapper driverPersonaGetPersonaBaseFromListWrapper = new DriverPersonaGetPersonaBaseFromListWrapper();
        driverPersonaGetPersonaBaseFromListWrapper.setArrayOfPersonaBase(arrayOfPersonaBase);
        return driverPersonaGetPersonaBaseFromListWrapper;
    }

    @GetMapping("/GetPersonaInfo")
    public DriverPersonaGetPersonaInfoWrapper getPersonaInfo(
            @RequestHeader("securityToken") String securityToken,
            @RequestHeader("userId") Long userId,
            @RequestParam("personaId") Long personaId) {
        SbPersona sbPersona = personaService.getPersonaById(personaId);
        DriverPersonaGetPersonaInfoWrapper driverPersonaGetPersonaInfoWrapper = new DriverPersonaGetPersonaInfoWrapper();
        ProfileDataOut profileData = new ProfileDataOut();
        BeanUtils.copyProperties(sbPersona, profileData);
        driverPersonaGetPersonaInfoWrapper.setProfileData(profileData);
        return driverPersonaGetPersonaInfoWrapper;
    }

    @PostMapping("/DeletePersona")
    public DriverPersonaDeletePersonaWrapper deletePersona(
            @RequestParam("personaId") Long personaId) {
        personaService.deletePersonaById(personaId);
        return new DriverPersonaDeletePersonaWrapper();
    }

    @PostMapping("/ReserveName")
    public DriverPersonaReserveNameWrapper reserveName(@RequestParam("name") String name) {
        DriverPersonaReserveNameWrapper driverPersonaReserveNameWrapper = new DriverPersonaReserveNameWrapper();
        ArrayOfStringOut arrayOfStringOut = new ArrayOfStringOut();
//        arrayOfStringOut.setString("NONE");
        driverPersonaReserveNameWrapper.setArrayOfStringOut(arrayOfStringOut);
        return driverPersonaReserveNameWrapper;
    }

}
