package world.soapboxrace.sbrace.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import world.soapboxrace.sbrace.controller.out.DragEventResultOut;
import world.soapboxrace.sbrace.controller.out.PursuitEventResultOut;
import world.soapboxrace.sbrace.controller.out.RouteEventResultOut;
import world.soapboxrace.sbrace.controller.out.TeamEscapeEventResultOut;
import world.soapboxrace.sbrace.controller.wrapper.EventArbitrationWrapper;
import world.soapboxrace.sbrace.controller.wrapper.EventArbitrationWrapperIn;
import world.soapboxrace.sbrace.service.PersonaService;

@RestController
@RequestMapping("/event")
@RequiredArgsConstructor
public class Event {

    private final PersonaService personaService;

    @PostMapping("/bust")
    public EventArbitrationWrapper bust(@RequestHeader("securityToken") String securityToken,
                                        @RequestHeader("userId") Long userId,
                                        @RequestBody EventArbitrationWrapperIn eventArbitrationIn,
                                        @RequestParam Long eventSessionId) {
        System.out.println(eventArbitrationIn);
        Long activePersonaId = personaService.getActivePersonaId(securityToken);
        EventArbitrationWrapper eventArbitrationWrapper = new EventArbitrationWrapper();
        PursuitEventResultOut pursuitEventResult = mockPursuitEventResult(eventSessionId, activePersonaId);
        eventArbitrationWrapper.setPursuitEventResult(pursuitEventResult);
        return eventArbitrationWrapper;
    }

    @PostMapping("/arbitration")
    public EventArbitrationWrapper arbitration(@RequestHeader("securityToken") String securityToken,
                                               @RequestHeader("userId") Long userId,
                                               @RequestBody EventArbitrationWrapperIn eventArbitrationIn,
                                               @RequestParam Long eventSessionId) {
        EventArbitrationWrapper eventArbitrationWrapper = new EventArbitrationWrapper();
        if (eventArbitrationIn.getDragArbitrationPacket() != null) {
            eventArbitrationWrapper.setDragEventResult(new DragEventResultOut());
        } else if (eventArbitrationIn.getRouteArbitrationPacket() != null) {
            eventArbitrationWrapper.setRouteEventResult(new RouteEventResultOut());
        } else if (eventArbitrationIn.getPursuitArbitrationPacket() != null) {
            Long activePersonaId = personaService.getActivePersonaId(securityToken);
            PursuitEventResultOut pursuitEventResult = mockPursuitEventResult(eventSessionId, activePersonaId);
            eventArbitrationWrapper.setPursuitEventResult(pursuitEventResult);
        } else if (eventArbitrationIn.getTeamEscapeArbitrationPacket() != null) {
            eventArbitrationWrapper.setTeamEscapeEventResult(new TeamEscapeEventResultOut());
        }
        return eventArbitrationWrapper;
    }

    private PursuitEventResultOut mockPursuitEventResult(Long eventSessionId, Long activePersonaId) {
        PursuitEventResultOut pursuitEventResult = new PursuitEventResultOut();
        pursuitEventResult.setHeat(1F);
        pursuitEventResult.setDurability(100);
//    pursuitEventResult.setEventId();
        pursuitEventResult.setEventSessionId(eventSessionId);
        pursuitEventResult.setExitPath("ExitToFreeroam");
        pursuitEventResult.setInviteLifetimeInMilliseconds(0L);
        pursuitEventResult.setLobbyInviteId(0L);
        pursuitEventResult.setPersonaId(activePersonaId);
        return pursuitEventResult;
    }

    @PutMapping("/launched")
    public void launched(@RequestParam Long eventSessionId) {
        System.out.println(eventSessionId);
    }

}
