package world.soapboxrace.sbrace.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.controller.out.*;
import world.soapboxrace.sbrace.controller.wrapper.EventsAvaliableAtLevelWrapper;
import world.soapboxrace.sbrace.controller.wrapper.EventsGetTreasureHuntEventSessionWrapper;
import world.soapboxrace.sbrace.data.entity.Event;
import world.soapboxrace.sbrace.service.BoostService;
import world.soapboxrace.sbrace.service.EventService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/events")
@RequiredArgsConstructor
public class Events {

    private final EventService eventService;

    private final BoostService boostService;

    @GetMapping("/availableatlevel")
    public EventsAvaliableAtLevelWrapper availableatlevel() {
        EventsAvaliableAtLevelWrapper eventsAvaliableAtLevelWrapper = new EventsAvaliableAtLevelWrapper();
        EventsPacketOut eventsPacket = new EventsPacketOut();
        eventsAvaliableAtLevelWrapper.setEventsPacket(eventsPacket);
        if (boostService.waitingPayment(0L)) {
            return eventsAvaliableAtLevelWrapper;
        }

        List<EventsOut> eventsOutList = new ArrayList<>();
        List<Event> allEvents = eventService.getAllEvents();
        allEvents.forEach(event -> {
            EventsOut eventDefinitionWithId = getEventDefinitionWithId(event.getId().intValue());
            eventDefinitionWithId.getEventDefinition().setEventModeId(event.getEventModeId());
            eventDefinitionWithId.getEventDefinition().setCarClassHash(event.getCarClassHash());
            eventsOutList.add(eventDefinitionWithId);
        });
        eventsPacket.setEvents(eventsOutList);
        return eventsAvaliableAtLevelWrapper;
    }

    private EventsOut getEventDefinitionWithId(Integer eventId) {
        EventsOut eventsOut = new EventsOut();
        EventDefinitionOut eventDefinition = new EventDefinitionOut();
        eventDefinition.setCarClassHash(607077938);
        eventDefinition.setCoins(0);
        EngagePointOut vector3 = new EngagePointOut();
        vector3.setX(0);
        vector3.setY(0);
        vector3.setZ(0);
        eventDefinition.setEngagePoint(vector3);
        eventDefinition.setEventId(eventId);
        eventDefinition.setEventLocalization(953954317);
        eventDefinition.setEventModeDescriptionLocalization(1298130000);
        eventDefinition.setEventModeIcon("GameModeIcon_Drag");
        eventDefinition.setEventModeId(19);
        eventDefinition.setEventModeLocalization(1990101845);
        eventDefinition.setIsEnabled(true);
        eventDefinition.setIsLocked(false);
        eventDefinition.setLaps(0);
        eventDefinition.setLength(0);
        eventDefinition.setMaxClassRating(999);
        eventDefinition.setMaxEntrants(4);
        eventDefinition.setMaxLevel(60);
        eventDefinition.setMinClassRating(0);
        eventDefinition.setMinEntrants(2);
        eventDefinition.setMinLevel(2);
        eventDefinition.setRegionLocalization(0);
        eventDefinition.setRewardModes("");
        eventDefinition.setTimeLimit(0);
        eventDefinition.setTrackLayoutMap("NeonCrossroads_128");
        eventDefinition.setTrackLocalization(0);
        eventsOut.setEventDefinition(eventDefinition);
        return eventsOut;
    }

    //gettreasurehunteventsession
    @GetMapping("/gettreasurehunteventsession")
    public EventsGetTreasureHuntEventSessionWrapper gettreasurehunteventsession() {
        EventsGetTreasureHuntEventSessionWrapper eventsGetTreasureHuntEventSessionWrapper = new EventsGetTreasureHuntEventSessionWrapper();
        TreasureHuntEventSessionOut treasureHuntEventSession = new TreasureHuntEventSessionOut();
        treasureHuntEventSession.setCoinsCollected(0);
        treasureHuntEventSession.setIsStreakBroken(true);
        treasureHuntEventSession.setNumCoins(0);
        treasureHuntEventSession.setSeed(-106502361);
        treasureHuntEventSession.setStreak(0);
        eventsGetTreasureHuntEventSessionWrapper.setTreasureHuntEventSession(treasureHuntEventSession);
        return eventsGetTreasureHuntEventSessionWrapper;
    }

}
