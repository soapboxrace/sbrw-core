package world.soapboxrace.sbrace.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.controller.out.ArrayOfLongOut;
import world.soapboxrace.sbrace.controller.wrapper.GetBlockedUserListWrapper;

@RestController
public class GetBlockedUserList {

    @GetMapping("/getblockeduserlist")
    public GetBlockedUserListWrapper getblockeduserlist() {
        GetBlockedUserListWrapper getBlockedUserListWrapper = new GetBlockedUserListWrapper();
        getBlockedUserListWrapper.setArrayOfLong(new ArrayOfLongOut());
        return getBlockedUserListWrapper;
    }
}
