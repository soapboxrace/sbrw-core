package world.soapboxrace.sbrace.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.controller.out.ArrayOfLongOut;
import world.soapboxrace.sbrace.controller.wrapper.GetBlockersByUsersWrapper;

@RestController
public class GetBlockersByUsers {

    @GetMapping("/getblockersbyusers")
    public GetBlockersByUsersWrapper getblockersbyusers(){
        GetBlockersByUsersWrapper getBlockersByUsersWrapper = new GetBlockersByUsersWrapper();
        getBlockersByUsersWrapper.setArrayOfLong(new ArrayOfLongOut());
        return getBlockersByUsersWrapper;
    }
}
