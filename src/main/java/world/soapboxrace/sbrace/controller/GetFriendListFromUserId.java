package world.soapboxrace.sbrace.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.controller.out.PersonaFriendsListOut;
import world.soapboxrace.sbrace.controller.wrapper.GetfriendListFromUserIdWrapper;

@RestController
public class GetFriendListFromUserId {

    @GetMapping("/getfriendlistfromuserid")
    public GetfriendListFromUserIdWrapper getfriendlistfromuserid() {
        GetfriendListFromUserIdWrapper getfriendListFromUserIdWrapper = new GetfriendListFromUserIdWrapper();
        getfriendListFromUserIdWrapper.setPersonaFriendsList(new PersonaFriendsListOut());
        return getfriendListFromUserIdWrapper;
    }

}
