package world.soapboxrace.sbrace.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.config.SbrwConfig;
import world.soapboxrace.sbrace.controller.out.ArrayOfUdpRelayInfoOut;
import world.soapboxrace.sbrace.controller.out.UdpRelayInfoOut;
import world.soapboxrace.sbrace.controller.wrapper.GetRebroadcastersWrapper;
import world.soapboxrace.sbrace.service.BoostService;

@RestController
@RequiredArgsConstructor
public class GetRebroadcasters {

    private final SbrwConfig sbrwConfig;
    private final BoostService boostService;

    @GetMapping("/getrebroadcasters")
    public GetRebroadcastersWrapper getRebroadcasters() {
        GetRebroadcastersWrapper getRebroadcastersWrapper = new GetRebroadcastersWrapper();
        ArrayOfUdpRelayInfoOut arrayOfUdpRelayInfo = new ArrayOfUdpRelayInfoOut();
        getRebroadcastersWrapper.setArrayOfUdpRelayInfo(arrayOfUdpRelayInfo);
        if (boostService.waitingPayment(0L)) {
            return getRebroadcastersWrapper;
        }
        UdpRelayInfoOut udpRelayInfo = new UdpRelayInfoOut();
        udpRelayInfo.setHost(sbrwConfig.getUdpFreeroamIp());
        udpRelayInfo.setPort(sbrwConfig.getUdpFreeroamPort());
        arrayOfUdpRelayInfo.setUdpRelayInfo(udpRelayInfo);
        return getRebroadcastersWrapper;
    }

}
