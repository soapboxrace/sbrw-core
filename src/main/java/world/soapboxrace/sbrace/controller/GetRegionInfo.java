package world.soapboxrace.sbrace.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.controller.out.RegionInfoOut;
import world.soapboxrace.sbrace.controller.wrapper.GetRegionInfoWrapper;

@RestController
public class GetRegionInfo {

    @GetMapping("/getregioninfo")
    public GetRegionInfoWrapper getRegionInfo() {
        GetRegionInfoWrapper getRegionInfoWrapper = new GetRegionInfoWrapper();
        RegionInfoOut regionInfo = new RegionInfoOut();
        regionInfo.setCountdownProposalInMilliseconds(3000);
        regionInfo.setDirectConnectTimeoutInMilliseconds(1000);
        regionInfo.setDropOutTimeInMilliseconds(15000);
        regionInfo.setEventLoadTimeoutInMilliseconds(30000);
        regionInfo.setHeartbeatIntervalInMilliseconds(1000);
        regionInfo.setUdpRelayBandwidthInBps(9600);
        regionInfo.setUdpRelayTimeoutInMilliseconds(60000);
        getRegionInfoWrapper.setRegionInfo(regionInfo);
        return getRegionInfoWrapper;
    }

}
