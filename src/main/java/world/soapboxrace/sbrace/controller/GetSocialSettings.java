package world.soapboxrace.sbrace.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.controller.out.SocialSettingsOut;
import world.soapboxrace.sbrace.controller.wrapper.GetSocialSettingsWrapper;

@RestController
public class GetSocialSettings {

    @GetMapping("/getsocialsettings")
    public GetSocialSettingsWrapper getsocialsettings() {
        GetSocialSettingsWrapper getSocialSettingsWrapper = new GetSocialSettingsWrapper();
        SocialSettingsOut socialSettings = new SocialSettingsOut();
        socialSettings.setAppearOffline(false);
        socialSettings.setDeclineGroupInvite(0);
        socialSettings.setDeclineIncommingFriendRequests(false);
        socialSettings.setDeclinePrivateInvite(0);
        socialSettings.setHideOfflineFriends(false);
        socialSettings.setShowNewsOnSignIn(false);
        socialSettings.setShowOnlyPlayersInSameChatChannel(false);
        getSocialSettingsWrapper.setSocialSettings(socialSettings);
        return getSocialSettingsWrapper;
    }
}
