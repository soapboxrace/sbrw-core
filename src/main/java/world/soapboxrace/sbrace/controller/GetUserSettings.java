package world.soapboxrace.sbrace.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.controller.out.UserSettingsOut;
import world.soapboxrace.sbrace.controller.wrapper.GetUserSettingsWrapper;

@RestController
public class GetUserSettings {

    @GetMapping("/getusersettings")
    public GetUserSettingsWrapper getUserSettings() {
        GetUserSettingsWrapper getUserSettingsWrapper = new GetUserSettingsWrapper();
        UserSettingsOut userSettings = new UserSettingsOut();
        userSettings.setCarCacheAgeLimit(600);
        userSettings.setIsRaceNowEnabled(true);
        userSettings.setMaxCarCacheSize(250);
        userSettings.setMinRaceNowLevel(2);
        userSettings.setVoipAvailable(false);
        userSettings.setFirstTimeLogin(false);
        userSettings.setMaxLevel(60);
        userSettings.setStarterPackApplied(false);
        userSettings.setUserId(6L);
        getUserSettingsWrapper.setUserSettings(userSettings);
        return getUserSettingsWrapper;
    }

}
