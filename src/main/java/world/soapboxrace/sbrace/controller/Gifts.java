package world.soapboxrace.sbrace.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.controller.wrapper.GiftsGetAndTriggerAvailableLevelGiftsWrapper;

@RestController
@RequestMapping("/Gifts")
public class Gifts {

    @PostMapping("/GetAndTriggerAvailableLevelGifts")
    public GiftsGetAndTriggerAvailableLevelGiftsWrapper getAndTriggerAvailableLevelGifts() {
        GiftsGetAndTriggerAvailableLevelGiftsWrapper giftsGetAndTriggerAvailableLevelGiftsWrapper = new GiftsGetAndTriggerAvailableLevelGiftsWrapper();
        giftsGetAndTriggerAvailableLevelGiftsWrapper.setArrayOfLevelGiftDefinition(new Object());
        return giftsGetAndTriggerAvailableLevelGiftsWrapper;
    }
}
