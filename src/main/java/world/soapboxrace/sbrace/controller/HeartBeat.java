package world.soapboxrace.sbrace.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.controller.wrapper.HeartBeatWrapper;

@RestController
public class HeartBeat {

    @PostMapping("/heartbeat")
    public HeartBeatWrapper heartbeat() {
        return new HeartBeatWrapper();
    }

}
