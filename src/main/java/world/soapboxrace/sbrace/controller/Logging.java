package world.soapboxrace.sbrace.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.controller.out.ClientConfigTransOut;
import world.soapboxrace.sbrace.controller.wrapper.LoggingClientWrapper;

@RestController
@RequestMapping("/logging")
public class Logging {

    @GetMapping("/client")
    public LoggingClientWrapper client() {
        LoggingClientWrapper loggingClientWrapper = new LoggingClientWrapper();
        loggingClientWrapper.setClientConfigTransOut(new ClientConfigTransOut());
        return loggingClientWrapper;
    }

}
