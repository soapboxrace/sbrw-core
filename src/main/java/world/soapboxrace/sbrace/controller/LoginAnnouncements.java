package world.soapboxrace.sbrace.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.controller.out.LoginAnnouncementsDefinitionOut;
import world.soapboxrace.sbrace.controller.wrapper.LoginAnnouncementsWrapper;

@RestController
public class LoginAnnouncements {

    @GetMapping("/LoginAnnouncements")
    public LoginAnnouncementsWrapper loginAnnouncements() {
        LoginAnnouncementsWrapper loginAnnouncementsWrapper = new LoginAnnouncementsWrapper();
        loginAnnouncementsWrapper.setLoginAnnouncementsDefinition(new LoginAnnouncementsDefinitionOut());
        return loginAnnouncementsWrapper;
    }

}
