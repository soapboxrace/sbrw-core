package world.soapboxrace.sbrace.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import world.soapboxrace.sbrace.controller.out.*;
import world.soapboxrace.sbrace.controller.wrapper.MatchmakingAcceptinviteWrapper;
import world.soapboxrace.sbrace.controller.wrapper.SessionInfoWrapper;
import world.soapboxrace.sbrace.service.LobbyService;
import world.soapboxrace.sbrace.session.LobbySession;
import world.soapboxrace.sbrace.session.LobbySessionEntrant;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/matchmaking")
@RequiredArgsConstructor
public class Matchmaking {

    private final LobbyService lobbyService;

    @PutMapping("/joinqueueevent/{eventId}")
    public void joinqueueevent(@RequestHeader("securityToken") String securityToken,
                               @RequestHeader("userId") Long userId,
                               @PathVariable("eventId") Integer eventId) {
        lobbyService.sendJoinEvent(securityToken, eventId);
    }

    @PutMapping("/acceptinvite")
    public MatchmakingAcceptinviteWrapper acceptinvite(@RequestHeader("securityToken") String securityToken,
                                                       @RequestHeader("userId") Long userId,
                                                       @RequestParam("lobbyInviteId") Long lobbyInviteId) {
        MatchmakingAcceptinviteWrapper matchmakingAcceptinviteWrapper = new MatchmakingAcceptinviteWrapper();
        LobbySession lobbySession = lobbyService.updateEntrants(securityToken, lobbyInviteId);
        List<LobbySessionEntrant> entrants = lobbySession.getEntrants();

        CountdownOut lobbyCountdown = new CountdownOut();
        lobbyCountdown.setLobbyId(lobbyInviteId);
        lobbyCountdown.setIsWaiting(false);
        lobbyCountdown.setEventId(lobbySession.getEventId());
        lobbyCountdown.setLobbyCountdownInMilliseconds(lobbySession.getTimeToLive());
        lobbyCountdown.setLobbyStuckDurationInMilliseconds(10000);
        LobbyInfoOut lobbyInfo = new LobbyInfoOut();
        lobbyInfo.setEntrants(new ArrayList<>());

        entrants.forEach(entrant -> {
            LobbyEntrantInfoOut lobbyEntrantInfo = new LobbyEntrantInfoOut();
            lobbyEntrantInfo.setPersonaId(entrant.getPersonaId());
            lobbyEntrantInfo.setLevel(entrant.getPersonaLevel());
            lobbyEntrantInfo.setGridIndex(entrant.getGridIndex());
            lobbyEntrantInfo.setHeat(entrant.getHeat());
            lobbyEntrantInfo.setState(entrant.getState());
            lobbyInfo.setCountdown(lobbyCountdown);
            EntrantsOut entrantsOut = new EntrantsOut();
            entrantsOut.setLobbyEntrantInfo(lobbyEntrantInfo);
            lobbyInfo.getEntrants().add(entrantsOut);
        });
        lobbyInfo.setEventId(lobbySession.getEventId());
        lobbyInfo.setLobbyId(lobbyInviteId);
        lobbyInfo.setLobbyInviteId(lobbyInviteId);
        lobbyInfo.setIsInviteEnabled(false);

        matchmakingAcceptinviteWrapper.setLobbyInfo(lobbyInfo);
        return matchmakingAcceptinviteWrapper;
    }


    @PutMapping("/leavelobby")
    public void leavelobby() {
    }

    @GetMapping("/launchevent/{eventId}")
    public SessionInfoWrapper launchEvent(@RequestHeader("securityToken") String securityToken,
                                          @RequestHeader("userId") Long userId,
                                          @PathVariable("eventId") Integer eventId) {
        SessionInfoWrapper sessionInfoWrapper = new SessionInfoWrapper();
        SessionInfoOut sessionInfo = new SessionInfoOut();
        sessionInfo.setSessionId(999L);
        sessionInfo.setEventId(eventId);
        sessionInfoWrapper.setSessionInfo(sessionInfo);
        return sessionInfoWrapper;
    }

}
