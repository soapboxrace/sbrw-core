package world.soapboxrace.sbrace.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.controller.out.ArrayOfNewsArticleTransOut;
import world.soapboxrace.sbrace.controller.wrapper.NewsArticlesWrapper;

@RestController
public class NewsArticles {

    @GetMapping("/NewsArticles")
    public NewsArticlesWrapper newsArticles() {
        NewsArticlesWrapper newsArticlesWrapper = new NewsArticlesWrapper();
        newsArticlesWrapper.setArrayOfNewsArticleTrans(new ArrayOfNewsArticleTransOut());
        return newsArticlesWrapper;
    }

}
