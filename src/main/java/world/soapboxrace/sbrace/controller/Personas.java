package world.soapboxrace.sbrace.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;
import world.soapboxrace.sbrace.component.CarOwnedCarDbToOut;
import world.soapboxrace.sbrace.component.OwnedCarInToDb;
import world.soapboxrace.sbrace.controller.in.OwnedCarTransIn;
import world.soapboxrace.sbrace.controller.out.*;
import world.soapboxrace.sbrace.controller.wrapper.*;
import world.soapboxrace.sbrace.data.entity.CarOwnedCar;
import world.soapboxrace.sbrace.data.entity.Inventory;
import world.soapboxrace.sbrace.data.entity.Product;
import world.soapboxrace.sbrace.service.BoostService;
import world.soapboxrace.sbrace.service.CarService;
import world.soapboxrace.sbrace.service.InventoryService;
import world.soapboxrace.sbrace.service.ProductService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/personas")
@RequiredArgsConstructor
public class Personas {

    private final ProductService productService;

    private final CarService carService;

    private final InventoryService inventoryService;

    private final BoostService boostService;

    @GetMapping("/{personaId}/carslots")
    public PersonasCarslotsWrapper carslots(@PathVariable("personaId") Long personaId) {
        List<CarsOwnedByPersonaOut> carsOwnedByPersonaList = new ArrayList<>();
        List<CarOwnedCar> personaCars = carService.getPersonaCars(personaId);
        personaCars.forEach(carOwnedCar -> {
            CarsOwnedByPersonaOut carsOwnedByPersonaOut = new CarsOwnedByPersonaOut();
            OwnedCarTransOut ownedCarTrans = CarOwnedCarDbToOut.copyOwnedCar(carOwnedCar);
            carsOwnedByPersonaOut.setOwnedCarTrans(ownedCarTrans);
            carsOwnedByPersonaList.add(carsOwnedByPersonaOut);
        });
        ObtainableSlotsOut obtainableSlots = getObtainableSlotsOut();
        CarSlotInfoTransOut carSlotInfoTrans = new CarSlotInfoTransOut();
        carSlotInfoTrans.setOwnedCarSlotsCount(30);
        carSlotInfoTrans.setDefaultOwnedCarIndex(0);
        carSlotInfoTrans.setObtainableSlots(obtainableSlots);
        carSlotInfoTrans.setCarsOwnedByPersona(carsOwnedByPersonaList);
        if (carsOwnedByPersonaList.isEmpty()) {
            carSlotInfoTrans.setCarsOwnedByPersona(null);
        }
        PersonasCarslotsWrapper personasCarslotsWrapper = new PersonasCarslotsWrapper();
        personasCarslotsWrapper.setCarSlotInfoTrans(carSlotInfoTrans);
        return personasCarslotsWrapper;
    }

    private ObtainableSlotsOut getObtainableSlotsOut() {
        ObtainableSlotsOut obtainableSlots = new ObtainableSlotsOut();
        ProductTransOut productTrans = new ProductTransOut();
        productTrans.setBundleItems("");
        productTrans.setCategoryId("");
        productTrans.setCurrency("_NS");
        productTrans.setDescription("New car slot !!");
        productTrans.setDurationMinute(0);
        productTrans.setHash(-1143680669);
        productTrans.setIcon("128_cash");
        productTrans.setLevel(70);
        productTrans.setLongDescription("New car slot !");
        productTrans.setPriority(0);
        productTrans.setProductId("SRV-CARSLOT");
        productTrans.setPrice(100F);
        productTrans.setVisualStyle("");
        productTrans.setSecondaryIcon("");
        productTrans.setUseCount(1);
        productTrans.setWebIcon("");
        productTrans.setWebLocation("");

        obtainableSlots.setProductTrans(productTrans);
        return obtainableSlots;
    }

    @GetMapping("/inventory/objects")
    public PersonasInventoryObjectsWrapper inventoryObjects(@RequestHeader("securityToken") String securityToken,
                                                            @RequestHeader("userId") Long userId) {
        boolean waitingPayment = boostService.waitingPayment(0L);
        Inventory inventory = inventoryService.getInventoryBySecurityToken(securityToken);
        InventoryTransOut inventoryTrans = new InventoryTransOut();
        BeanUtils.copyProperties(inventory, inventoryTrans);
        List<InventoryItemsOut> inventoryItems = new ArrayList<>();
        inventory.getItems().forEach(inventoryItem -> {
            InventoryItemsOut inventoryItemsOut = new InventoryItemsOut();
            InventoryItemTransOut inventoryItemTrans = new InventoryItemTransOut();
            BeanUtils.copyProperties(inventoryItem, inventoryItemTrans);
            if (waitingPayment) {
                System.out.println("waiting payment");
                inventoryItemTrans.setRemainingUseCount(0);
            }
            inventoryItemsOut.setInventoryItemTrans(inventoryItemTrans);
            inventoryItems.add(inventoryItemsOut);
        });
        inventoryTrans.setInventoryItems(inventoryItems);
        PersonasInventoryObjectsWrapper personasInventoryObjectsWrapper = new PersonasInventoryObjectsWrapper();
        personasInventoryObjectsWrapper.setInventoryTrans(inventoryTrans);
        return personasInventoryObjectsWrapper;
    }

    @PutMapping("/{personaId}/defaultcar/{carId}")
    public void putDefaultCar(@RequestHeader("securityToken") String securityToken,
                              @RequestHeader("userId") Long userId,
                              @PathVariable("personaId") Long personaId,
                              @PathVariable("carId") Long carId) {
        carService.setDefaultCar(personaId, carId);
    }

    @PostMapping("/{personaId}/cars")
    public PersonasDefaultcarWrapper sellCar(@PathVariable("personaId") Long personaId,
                                             @RequestParam("serialNumber") Long carId) {
        carService.deleteCar(carId);
        return getDefaultCar(personaId);
    }

    // remove skill or performance from customization menu
    @PutMapping("/{personaId}/cars")
    public PersonaCarsWrapperIn putCustomization(@RequestBody PersonaCarsWrapperIn personaCarsWrapperIn,
                                                 @PathVariable("personaId") Long personaId) {
        OwnedCarTransIn ownedCarTrans = personaCarsWrapperIn.getOwnedCarTrans();
        ownedCarTrans.setDurability(100);
        ownedCarTrans.setHeat(1F);
        return personaCarsWrapperIn;
    }

    // update all car from customization menu
    @PostMapping("/{personaId}/commerce")//PersonaCarWrapperIn
    public PersonasCommerceWrapper postCustomization(@RequestBody PersonaCommerceWrapperIn personaCommerceWrapperIn,
                                                     @PathVariable("personaId") Long personaId) {
        OwnedCarTransIn updatedCar = personaCommerceWrapperIn.getCommerceSessionTrans().getUpdatedCar();
        CarOwnedCar carOwnedCar = OwnedCarInToDb.copyOwnedCar(updatedCar);
        carOwnedCar = carService.saveCar(personaId, carOwnedCar);
        OwnedCarTransOut ownedCarTransOut = CarOwnedCarDbToOut.copyOwnedCar(carOwnedCar);
        PersonasCommerceWrapper personasCommerceWrapper = new PersonasCommerceWrapper();
        CommerceResultTransOut commerceResultTrans = new CommerceResultTransOut();
        InventoryItemsOut inventoryItems = new InventoryItemsOut();
        InventoryItemTransOut itemTransOut = new InventoryItemTransOut();
        itemTransOut.setHash(0);
        itemTransOut.setInventoryId(0L);
        itemTransOut.setRemainingUseCount(0);
        itemTransOut.setResellPrice(0F);
        inventoryItems.setInventoryItemTrans(itemTransOut);
        commerceResultTrans.setInventoryItems(inventoryItems);
        WalletsOut wallets = new WalletsOut();
        WalletTransOut walletTrans = new WalletTransOut();
        walletTrans.setCurrency("CASH");
        walletTrans.setBalance(5000000F);
        wallets.setWalletTrans(walletTrans);
        commerceResultTrans.setWallets(wallets);
        commerceResultTrans.setUpdatedCar(ownedCarTransOut);
        personasCommerceWrapper.setCommerceResultTrans(commerceResultTrans);
        return personasCommerceWrapper;
    }

    @GetMapping("/{personaId}/cars")
    public PersonaCarsWrapper cars(@PathVariable("personaId") Long personaId) {
        PersonaCarsWrapper personaCarsWrapper = new PersonaCarsWrapper();
        List<CarsOwnedByPersonaOut> arrayOfOwnedCarTrans = new ArrayList<>();
        List<CarOwnedCar> personaCars = carService.getPersonaCars(personaId);
        personaCars.forEach(carOwnedCar -> {
            CarsOwnedByPersonaOut carsOwnedByPersonaOut = new CarsOwnedByPersonaOut();
            OwnedCarTransOut ownedCarTrans = CarOwnedCarDbToOut.copyOwnedCar(carOwnedCar);
            carsOwnedByPersonaOut.setOwnedCarTrans(ownedCarTrans);
            arrayOfOwnedCarTrans.add(carsOwnedByPersonaOut);
        });
        personaCarsWrapper.setArrayOfOwnedCarTrans(arrayOfOwnedCarTrans);
        return personaCarsWrapper;
    }

    @PostMapping("/{personaId}/baskets")
    public PersonasBasketsWrapper baskets(
            @RequestHeader("securityToken") String securityToken,
            @RequestHeader("userId") Long userId,
            @RequestBody PersonasBasketsWrapperIn personasBasketsWrapperIn,
            @PathVariable("personaId") Long personaId) {
        String productId = personasBasketsWrapperIn.getBasketTrans().getItems().getBasketItemTrans().getProductId();
        Product product = productService.getByProductId(productId);
        if ("PRESETCAR".equals(product.getProductType())) {
            carService.buyCar(personaId, product);
        }
        PersonasBasketsWrapper personasBasketsWrapper = new PersonasBasketsWrapper();
        CommerceResultTransOut commerceResultTrans = new CommerceResultTransOut();
        InventoryItemsOut inventoryItems = new InventoryItemsOut();
        InventoryItemTransOut itemTransOut = new InventoryItemTransOut();
        itemTransOut.setHash(0);
        itemTransOut.setInventoryId(0L);
        itemTransOut.setRemainingUseCount(0);
        itemTransOut.setResellPrice(0F);
        inventoryItems.setInventoryItemTrans(itemTransOut);
        commerceResultTrans.setInventoryItems(inventoryItems);
        PurchasedCarsOut purchasedCars = new PurchasedCarsOut();
        OwnedCarTransOut ownedCarTrans = new OwnedCarTransOut();
        ownedCarTrans.setDurability(0);
        ownedCarTrans.setHeat(0F);
        ownedCarTrans.setId(0L);
        purchasedCars.setOwnedCarTrans(ownedCarTrans);
        commerceResultTrans.setPurchasedCars(purchasedCars);
        WalletsOut wallets = new WalletsOut();
        WalletTransOut walletTrans = new WalletTransOut();
        walletTrans.setCurrency("CASH");
        walletTrans.setBalance(5000000F);
        wallets.setWalletTrans(walletTrans);
        commerceResultTrans.setWallets(wallets);
        personasBasketsWrapper.setCommerceResultTrans(commerceResultTrans);

        if ("-1".equals(productId) || "SRV-CARSLOT".equals(productId) || "SRV-THREVIVE".equals(productId)) {
            commerceResultTrans.setStatus("Fail_InsufficientFunds");
        }
        return personasBasketsWrapper;
    }

    @GetMapping(value = "/{personaId}/defaultcar")
    public PersonasDefaultcarWrapper getDefaultCar(@PathVariable("personaId") Long personaId) {
        CarOwnedCar carOwnedCar = carService.getPersonaDefaultCar(personaId);
        PersonasDefaultcarWrapper personasDefaultcarWrapper = new PersonasDefaultcarWrapper();
        if (carOwnedCar == null) {
            personasDefaultcarWrapper.setOwnedCarTrans(new OwnedCarTransOut());
            return personasDefaultcarWrapper;
        }
        OwnedCarTransOut ownedCarTransOut = CarOwnedCarDbToOut.copyOwnedCar(carOwnedCar);
        personasDefaultcarWrapper.setOwnedCarTrans(ownedCarTransOut);
        return personasDefaultcarWrapper;
    }

}
