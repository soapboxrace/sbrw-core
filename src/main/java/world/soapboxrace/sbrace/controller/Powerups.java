package world.soapboxrace.sbrace.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import world.soapboxrace.sbrace.service.PersonaService;
import world.soapboxrace.sbrace.xmpp.SbXmppCliServiceProxy;

@RestController
@RequestMapping("/powerups")
@RequiredArgsConstructor
public class Powerups {

    private final SbXmppCliServiceProxy sbXmppCliServiceProxy;
    private final PersonaService personaService;

    @PostMapping(value = "/activated/{powerupHash}")
    public void activated(@RequestHeader("securityToken") String securityToken,
                          @PathVariable Integer powerupHash,
                          @RequestParam("targetId") Long targetId,
                          @RequestParam("receivers") String receivers) {
        Long activePersonaId = personaService.getActivePersonaId(securityToken);
        sbXmppCliServiceProxy.powerupActivated(activePersonaId, targetId, powerupHash, receivers);
    }

}
