package world.soapboxrace.sbrace.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/Reporting")
public class Reporting {

    @PostMapping("/SendUserSettings")
    public void sendUserSettings() {
    }

    @PostMapping("/SendHardwareInfo")
    public void sendHardwareInfo() {
    }

}
