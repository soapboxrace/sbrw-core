package world.soapboxrace.sbrace.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.controller.out.FraudConfigOut;
import world.soapboxrace.sbrace.controller.wrapper.SecurityFraudConfigWrapper;

@RestController
@RequestMapping("/security")
public class Security {

    @GetMapping("/fraudConfig")
    public SecurityFraudConfigWrapper fraudConfig() {
        SecurityFraudConfigWrapper securityFraudConfigWrapper = new SecurityFraudConfigWrapper();
        FraudConfigOut fraudConfig = new FraudConfigOut();
        fraudConfig.setEnabledBitField(12);
        fraudConfig.setGameFileFreq(1000000);
        fraudConfig.setModuleFreq(360000);
        fraudConfig.setStartUpFreq(1000000);
        fraudConfig.setUserID(6L);
        securityFraudConfigWrapper.setFraudConfig(fraudConfig);
        return securityFraudConfigWrapper;
    }

    @PostMapping("/generateWebToken")
    public void generateWebToken(){

    }

}
