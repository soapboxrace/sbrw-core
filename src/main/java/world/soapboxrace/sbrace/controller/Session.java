package world.soapboxrace.sbrace.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.config.SbrwConfig;
import world.soapboxrace.sbrace.controller.out.ChatServerOut;
import world.soapboxrace.sbrace.controller.wrapper.SessionGetChatInfoWrapper;

@RestController
@RequestMapping("/Session")
@RequiredArgsConstructor
public class Session {

    private final SbrwConfig sbrwConfig;

    @GetMapping("/GetChatInfo")
    public SessionGetChatInfoWrapper getChatInfo() {
        SessionGetChatInfoWrapper sessionGetChatInfoWrapper = new SessionGetChatInfoWrapper();
        ChatServerOut chatServerOut = new ChatServerOut();
        chatServerOut.setPort(sbrwConfig.getXmppPort());
        chatServerOut.setIp(sbrwConfig.getXmppFqdn());
        chatServerOut.setPrefix("sbrw");
        sessionGetChatInfoWrapper.setChatServer(chatServerOut);
        return sessionGetChatInfoWrapper;
    }

}
