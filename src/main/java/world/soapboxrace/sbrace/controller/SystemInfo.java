package world.soapboxrace.sbrace.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import world.soapboxrace.sbrace.controller.out.SystemInfoOut;
import world.soapboxrace.sbrace.controller.wrapper.SystemInfoWrapper;

import java.time.LocalDateTime;

@RestController
public class SystemInfo {

    @GetMapping("/systeminfo")
    public SystemInfoWrapper systeminfo() {
        SystemInfoWrapper systemInfoWrapper = new SystemInfoWrapper();
        SystemInfoOut systemInfoOut = new SystemInfoOut();
        systemInfoOut.setClientVersionCheck(true);
        systemInfoOut.setEntitlementsToDownload(true);
        systemInfoOut.setForcePermanentSession(true);
        systemInfoOut.setPersonaCacheTimeout(900);
        systemInfoOut.setTime(LocalDateTime.now());
        systemInfoWrapper.setSystemInfoOut(systemInfoOut);
        return systemInfoWrapper;
    }
}
