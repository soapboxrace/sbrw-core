package world.soapboxrace.sbrace.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;
import world.soapboxrace.sbrace.controller.out.PersonaOut;
import world.soapboxrace.sbrace.controller.out.UserInfoOut;
import world.soapboxrace.sbrace.controller.out.UserOut;
import world.soapboxrace.sbrace.controller.wrapper.AuthenticateUserWrapper;
import world.soapboxrace.sbrace.controller.wrapper.GetPermanentSessionWrapper;
import world.soapboxrace.sbrace.controller.wrapper.PersonaWrapper;
import world.soapboxrace.sbrace.data.entity.SbPersona;
import world.soapboxrace.sbrace.data.entity.SbUser;
import world.soapboxrace.sbrace.service.UserService;
import world.soapboxrace.sbrace.session.TokenSessionService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/User")
@RequiredArgsConstructor
public class User {

    private final UserService userService;

    @GetMapping(path = "/authenticateUser")
    public AuthenticateUserWrapper authenticateUser(
            @RequestParam("email") String email,
            @RequestParam("password") String password) {
        SbUser sbUser = userService.getSbUserByEmail(email);
        AuthenticateUserWrapper authenticateUserWrapper = new AuthenticateUserWrapper();
        AuthenticateUserWrapper.LoginStatusVO loginStatusVO = new AuthenticateUserWrapper.LoginStatusVO();
        if (sbUser != null && password.equals(sbUser.getPassword())) {
            Long userId = sbUser.getUserId();
            String loginToken = userService.createTemporarySession(userId);
            loginStatusVO.setUserId(userId);
            loginStatusVO.setLoginToken(loginToken);
            loginStatusVO.setDescription("");
        }
        authenticateUserWrapper.setLoginStatusVO(loginStatusVO);
        return authenticateUserWrapper;
    }

    @PostMapping(path = "/GetPermanentSession")
    public GetPermanentSessionWrapper getPermanentSession(
            @RequestHeader("securityToken") String securityToken,
            @RequestHeader("userId") Long userId) {
        String permanentToken = userService.createPermanentSession(userId, securityToken);

        UserInfoOut userInfoOut = new UserInfoOut();
        userInfoOut.setPersonas(new ArrayList<>());
        List<Long> personasIds = new ArrayList<>();
        List<SbPersona> listSbPersona = userService.getSbPersonasByUserId(userId);
        listSbPersona.forEach(sbPersona -> {
            PersonaOut persona = new PersonaOut();
            BeanUtils.copyProperties(sbPersona, persona);
            PersonaWrapper personaWrapper = new PersonaWrapper();
            personaWrapper.setPersona(persona);
            userInfoOut.getPersonas().add(personaWrapper);
            personasIds.add(sbPersona.getPersonaId());
        });

        userService.createAllPersonasXmpp(personasIds, permanentToken);
        GetPermanentSessionWrapper getPermanentSessionWrapper = new GetPermanentSessionWrapper();

        userInfoOut.setDefaultPersonaIdx(0);
        UserOut userOut = new UserOut();
        userOut.setUserId(userId);
        userOut.setRemoteUserId("0");
        userOut.setIsComplete(false);
        userOut.setSecurityToken(permanentToken);
        userOut.setFullGameAccess(false);
        userOut.setSubscribeMsg(false);
        userInfoOut.setUser(userOut);
        getPermanentSessionWrapper.setUserInfo(userInfoOut);
        return getPermanentSessionWrapper;
    }

    @PostMapping("/SecureLoginPersona")
    public void secureLoginPersona(
            @RequestHeader("securityToken") String securityToken,
            @RequestHeader("userId") Long userId,
            @RequestParam("personaId") Long personaId) {
        userService.setActivePersona(securityToken, personaId);
    }

    @PostMapping("/SecureLogout")
    public void secureLogout() {
    }

    @PostMapping("/SecureLogoutPersona")
    public void secureLogoutPersona() {
    }

}
