package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseArbitrationPacketIn {

    @JsonProperty("AlternateEventDurationInMilliseconds")
    private Long AlternateEventDurationInMilliseconds;

    @JsonProperty("CarId")
    private Long CarId;

    @JsonProperty("EventDurationInMilliseconds")
    private Long EventDurationInMilliseconds;

    @JsonProperty("FinishReason")
    private Integer FinishReason;

    @JsonProperty("HacksDetected")
    private Integer HacksDetected;

    @JsonProperty("Rank")
    private Integer Rank;

    @JsonProperty("LongestJumpDurationInMilliseconds")
    private Long LongestJumpDurationInMilliseconds;

    @JsonProperty("SumOfJumpsDurationInMilliseconds")
    private Long SumOfJumpsDurationInMilliseconds;

    @JsonProperty("TopSpeed")
    private Float TopSpeed;

    @JsonProperty("PhysicsMetrics")
    private PhysicsMetricsIn physicsMetrics;

    @JsonProperty("FraudDetectionInfo")
    private FraudDetectionInfoIn fraudDetectionInfoIn;

}
