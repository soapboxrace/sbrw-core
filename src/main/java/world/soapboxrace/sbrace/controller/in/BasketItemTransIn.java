package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasketItemTransIn {

    @JsonProperty("ProductId")
    private String ProductId;

    @JsonProperty("Quantity")
    private Integer Quantity;

}
