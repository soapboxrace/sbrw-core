package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomCarIn {

    @JsonProperty("BaseCar")
    private Integer BaseCar;

    @JsonProperty("CarClassHash")
    private Integer CarClassHash;

    @JsonProperty("Id")
    private Long Id;

    @JsonProperty("IsPreset")
    private Boolean IsPreset;

    @JsonProperty("Level")
    private Integer Level;

    @JsonProperty("Name")
    private Object Name;

    @JsonProperty("Paints")
    private PaintsIn Paints;

    @JsonProperty("PerformanceParts")
    private PerformancePartsIn PerformanceParts;

    @JsonProperty("PhysicsProfileHash")
    private Integer PhysicsProfileHash;

    @JsonProperty("Rating")
    private Integer Rating;

    @JsonProperty("ResalePrice")
    private Float ResalePrice;

    @JsonProperty("RideHeightDrop")
    private Float RideHeightDrop;

    @JsonProperty("SkillModParts")
    private SkillModPartsIn SkillModParts;

    @JsonProperty("SkillModSlotCount")
    private Integer SkillModSlotCount;

    @JsonProperty("Version")
    private Integer Version;

    @JsonProperty("Vinyls")
    private VinylsIn Vinyls;

    @JsonProperty("VisualParts")
    private VisualPartsIn VisualParts;

}
