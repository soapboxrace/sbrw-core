package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DragArbitrationPacketIn extends BaseArbitrationPacketIn {

    @JsonProperty("FractionCompleted")
    private Float FractionCompleted;

    @JsonProperty("NumberOfCollisions")
    private Integer NumberOfCollisions;

    @JsonProperty("PerfectStart")
    private Integer PerfectStart;

}
