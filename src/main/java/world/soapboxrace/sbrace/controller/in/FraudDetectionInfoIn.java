package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FraudDetectionInfoIn {

    @JsonProperty("Checksum")
    private Integer Checksum;

    @JsonProperty("IntValue1")
    private Integer IntValue1;

    @JsonProperty("IntValue2")
    private Integer IntValue2;

    @JsonProperty("IntValue3")
    private Integer IntValue3;

    @JsonProperty("IntValue4")
    private Integer IntValue4;

    @JsonProperty("IsEncrypted")
    private Boolean IsEncrypted;

    @JsonProperty("ModuleName")
    private String ModuleName;

    @JsonProperty("ModulePath")
    private String ModulePath;

    @JsonProperty("ModuleValue")
    private Integer ModuleValue;

    @JsonProperty("StringValue1")
    private Object StringValue1;

    @JsonProperty("StringValue2")
    private Object StringValue2;
}
