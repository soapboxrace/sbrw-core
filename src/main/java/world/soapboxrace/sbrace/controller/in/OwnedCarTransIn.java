package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OwnedCarTransIn {

    @JsonProperty("CustomCar")
    private CustomCarIn CustomCar;

    @JsonProperty("Durability")
    private Integer Durability;

    @JsonProperty("Heat")
    private Float Heat;

    @JsonProperty("Id")
    private Long Id;

    @JsonProperty("OwnershipType")
    private Object OwnershipType;

}
