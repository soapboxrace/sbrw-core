package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.component.ObjectToList;
import world.soapboxrace.sbrace.controller.out.CustomPaintTransOut;

import java.util.List;

@Getter
@Setter
public class PaintsIn {

    @JsonProperty("CustomPaintTrans")
    private List<CustomPaintTransOut> customPaintTrans;

    @JsonCreator
    public PaintsIn(@JsonProperty("CustomPaintTrans") Object objectIn) {
        this.customPaintTrans = ObjectToList.convertToList(objectIn, CustomPaintTransOut.class);
    }

}
