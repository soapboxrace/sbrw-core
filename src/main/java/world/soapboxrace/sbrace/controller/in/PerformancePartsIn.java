package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.component.ObjectToList;
import world.soapboxrace.sbrace.controller.out.PerformancePartTransOut;

import java.util.List;

@Getter
@Setter
public class PerformancePartsIn {

    @JsonProperty("PerformancePartTrans")
    private List<PerformancePartTransOut> performancePartTrans;

    @JsonCreator
    public PerformancePartsIn(@JsonProperty("PerformancePartTrans") Object objectIn) {
        this.performancePartTrans = ObjectToList.convertToList(objectIn, PerformancePartTransOut.class);
    }

}
