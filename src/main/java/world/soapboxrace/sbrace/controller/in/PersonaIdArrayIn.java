package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonaIdArrayIn {

    @JsonProperty("PersonaIds")
    private PersonaIdsIn PersonaIds;

}
