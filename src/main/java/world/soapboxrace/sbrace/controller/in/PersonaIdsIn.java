package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class PersonaIdsIn {

    @JsonProperty("array:long")
    private List<Long> arrayLong;

    @JsonCreator
    public PersonaIdsIn(@JsonProperty("array:long") Object arrayLong) {
        this.arrayLong = convertToList(arrayLong);
    }

    private List<Long> convertToList(Object valorLista) {
        List<Long> lista = new ArrayList<>();
        if (valorLista instanceof List) {
            for (Object obj : (List<?>) valorLista) {
                lista.add(Long.valueOf(obj.toString()));
            }
        } else {
            lista.add(Long.valueOf(valorLista.toString()));
        }
        return lista;
    }
}
