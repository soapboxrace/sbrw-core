package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PhysicsMetricsIn {

    @JsonProperty("AccelerationAverage")
    private Float AccelerationAverage;

    @JsonProperty("AccelerationMaximum")
    private Float AccelerationMaximum;

    @JsonProperty("AccelerationMedian")
    private Float AccelerationMedian;

    @JsonProperty("SpeedAverage")
    private Float SpeedAverage;

    @JsonProperty("SpeedMaximum")
    private Float SpeedMaximum;

    @JsonProperty("SpeedMedian")
    private Float SpeedMedian;
}
