package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PursuitArbitrationPacketIn extends BaseArbitrationPacketIn {

    @JsonProperty("CopsDeployed")
    private Integer CopsDeployed;

    @JsonProperty("CopsDisabled")
    private Integer CopsDisabled;

    @JsonProperty("CopsRammed")
    private Integer CopsRammed;

    @JsonProperty("CostToState")
    private Long CostToState;

    @JsonProperty("Heat")
    private Float Heat;

    @JsonProperty("Infractions")
    private Integer Infractions;

    @JsonProperty("RoadBlocksDodged")
    private Integer RoadBlocksDodged;

    @JsonProperty("SpikeStripsDodged")
    private Integer SpikeStripsDodged;

}
