package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RouteArbitrationPacketIn extends DragArbitrationPacketIn {

    @JsonProperty("BestLapDurationInMilliseconds")
    private Long BestLapDurationInMilliseconds;

}
