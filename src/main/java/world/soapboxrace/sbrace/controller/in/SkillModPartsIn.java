package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.component.ObjectToList;
import world.soapboxrace.sbrace.controller.out.SkillModPartTransOut;

import java.util.List;

@Getter
@Setter
public class SkillModPartsIn {

    @JsonProperty("SkillModPartTrans")
    private List<SkillModPartTransOut> skillModPartTrans;

    @JsonCreator
    public SkillModPartsIn(@JsonProperty("SkillModPartTrans") Object objectIn) {
        this.skillModPartTrans = ObjectToList.convertToList(objectIn, SkillModPartTransOut.class);
    }

}
