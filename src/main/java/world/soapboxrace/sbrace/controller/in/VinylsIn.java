package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.component.ObjectToList;
import world.soapboxrace.sbrace.controller.out.CustomVinylTransOut;

import java.util.List;

@Getter
@Setter
public
class VinylsIn {

    @JsonProperty("CustomVinylTrans")
    private List<CustomVinylTransOut> customVinylTrans;

    @JsonCreator
    public VinylsIn(@JsonProperty("CustomVinylTrans") Object objectIn) {
        this.customVinylTrans = ObjectToList.convertToList(objectIn, CustomVinylTransOut.class);
    }

}
