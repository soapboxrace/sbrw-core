package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.component.ObjectToList;
import world.soapboxrace.sbrace.controller.out.VisualPartTransOut;

import java.util.List;

@Getter
@Setter
public class VisualPartsIn {

    @JsonProperty("VisualPartTrans")
    private List<VisualPartTransOut> visualPartTrans;

    @JsonCreator
    public VisualPartsIn(@JsonProperty("VisualPartTrans") Object objectIn) {
        this.visualPartTrans = ObjectToList.convertToList(objectIn, VisualPartTransOut.class);
    }

}
