package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public
class AccoladesOut {

    @JsonProperty("FinalRewards")
    private FinalRewardsOut FinalRewards;

    @JsonProperty("HasLeveledUp")
    private Boolean HasLeveledUp;

    @JsonProperty("LuckyDrawInfo")
    private LuckyDrawInfoOut LuckyDrawInfo;

    @JsonProperty("OriginalRewards")
    private OriginalRewardsOut OriginalRewards;

    @JsonProperty("RewardInfo")
    private List<RewardInfoOut> rewardInfoList;

}
