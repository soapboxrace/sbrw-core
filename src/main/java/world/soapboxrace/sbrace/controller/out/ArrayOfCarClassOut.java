package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ArrayOfCarClassOut {

    @JsonProperty("CarClass")
    private CarClassOut carClass;

}
