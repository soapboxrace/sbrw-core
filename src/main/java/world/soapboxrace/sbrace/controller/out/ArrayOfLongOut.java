package world.soapboxrace.sbrace.controller.out;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ArrayOfLongOut {

    private AttrOut2 _attr = new AttrOut2(
            "http://www.w3.org/2001/XMLSchema-instance",
            "http://schemas.microsoft.com/2003/10/Serialization/Arrays");

}
