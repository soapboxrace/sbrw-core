package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ArrayOfPersonaBaseOut {

    @JsonProperty("PersonaBase")
    private PersonaBaseOut personaBaseOut;
    
}
