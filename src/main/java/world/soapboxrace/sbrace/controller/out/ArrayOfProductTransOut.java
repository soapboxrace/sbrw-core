package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ArrayOfProductTransOut {

    @JsonProperty("ProductTrans")
    private List<ProductTransOutWrapper> productTrans;

}
