package world.soapboxrace.sbrace.controller.out;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ArrayOfStringOut {

    private String string;

}
