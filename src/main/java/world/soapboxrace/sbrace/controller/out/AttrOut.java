package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AttrOut {

    @JsonProperty("xsi:schemaLocation")
    private String xsiSchemaLocation;

    @JsonProperty("xmlns:xsi")
    private String xmlnsXsi;

}
