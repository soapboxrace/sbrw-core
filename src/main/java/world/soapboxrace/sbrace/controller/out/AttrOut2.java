package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AttrOut2 {

    @JsonProperty("xmlns:i")
    private String xmlnsI;

    @JsonProperty("xmlns")
    private String xmlns;

}
