package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class BaseEventResultOut {

    @JsonProperty("Accolades")
    private AccoladesOut Accolades;

    @JsonProperty("Durability")
    private Integer Durability;

    @JsonProperty("EventId")
    private Integer EventId;

    @JsonProperty("EventSessionId")
    private Long EventSessionId;

    @JsonProperty("ExitPath")
    private String ExitPath;

    @JsonProperty("InviteLifetimeInMilliseconds")
    private Long InviteLifetimeInMilliseconds;

    @JsonProperty("LobbyInviteId")
    private Long LobbyInviteId;

    @JsonProperty("PersonaId")
    private Long PersonaId;

    @JsonProperty("Entrants")
    private List<EventResultEntrantsOut> entrantsList;

}
