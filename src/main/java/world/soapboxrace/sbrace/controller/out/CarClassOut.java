package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarClassOut {

    @JsonProperty("CarClassHash")
    private Integer CarClassHash;

    @JsonProperty("MaxRating")
    private Integer MaxRating;

    @JsonProperty("MinRating")
    private Integer MinRating;

}
