package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CarSlotInfoTransOut {

    @JsonProperty("CarsOwnedByPersona")
    private List<CarsOwnedByPersonaOut> CarsOwnedByPersona;

    @JsonProperty("ObtainableSlots")
    private ObtainableSlotsOut ObtainableSlots;

    @JsonProperty("DefaultOwnedCarIndex")
    private Integer DefaultOwnedCarIndex;

    @JsonProperty("OwnedCarSlotsCount")
    private Integer OwnedCarSlotsCount;

}
