package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CategoryTransOut {

    @JsonProperty("CatalogVersion")
    String CatalogVersion = "20136";

    @JsonProperty("Categories")
    String Categories = "";

    @JsonProperty("DisplayName")
    String DisplayName = "";

    @JsonProperty("FilterType")
    Integer FilterType = 0;

    @JsonProperty("Icon")
    String Icon = "";

    @JsonProperty("Id")
    Integer hash;

    @JsonProperty("LongDescription")
    String LongDescription = "";

    @JsonProperty("Name")
    String Name;

    @JsonProperty("Priority")
    Integer Priority;

    @JsonProperty("ShortDescription")
    String ShortDescription = "";

    @JsonProperty("ShowInNavigationPane")
    Boolean ShowInNavigationPane = false;

    @JsonProperty("ShowPromoPage")
    Boolean ShowPromoPage = false;

    @JsonProperty("WebIcon")
    String WebIcon = "";

    @JsonProperty("Products")
    private List<ProductsOut> products;

}
