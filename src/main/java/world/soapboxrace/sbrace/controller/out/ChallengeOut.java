package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChallengeOut {

    @JsonProperty("ChallengeId")
    private String ChallengeId = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

    @JsonProperty("LeftSize")
    private Integer LeftSize = 14;

    @JsonProperty("Pattern")
    private String Pattern = "FFFFFFFFFFFFFFFF";

    @JsonProperty("RightSize")
    private Integer RightSize = 50;
}
