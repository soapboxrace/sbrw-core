package world.soapboxrace.sbrace.controller.out;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChatRoomOut {

    private Integer channelCount = 2;
    private String longName = "TXT_CHAT_LANG_ENGLISH";
    private String shortName = "EN";
}
