package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChatServerOut {

    @JsonProperty("Rooms")
    private RoomsOut rooms = new RoomsOut();
    private Integer port;
    private String ip;
    private String prefix;

}
