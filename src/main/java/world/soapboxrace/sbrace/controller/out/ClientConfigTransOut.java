package world.soapboxrace.sbrace.controller.out;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientConfigTransOut {

    private AttrOut _attr = new AttrOut(
            "http://schemas.datacontract.org/2004/07/Victory.DataLayer.Serialization"
            ,"http://www.w3.org/2001/XMLSchema-instance");

}
