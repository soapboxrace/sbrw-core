package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientServerCryptoTicketOut {

    @JsonProperty("CryptoTicket")
    private String CryptoTicket;

    @JsonProperty("SessionKey")
    private String SessionKey;

    @JsonProperty("TicketIv")
    private String TicketIv;

}
