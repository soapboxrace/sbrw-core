package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommerceResultTransOut {

    @JsonProperty("CommerceItems")
    private String CommerceItems = "";

    @JsonProperty("InvalidBasket")
    private String InvalidBasket = "";

    @JsonProperty("Status")
    private String Status = "Success";

    @JsonProperty("InventoryItems")
    private InventoryItemsOut InventoryItems;

    @JsonProperty("PurchasedCars")
    private PurchasedCarsOut PurchasedCars;

    @JsonProperty("Wallets")
    private WalletsOut Wallets;

    @JsonProperty("UpdatedCar")
    private OwnedCarTransOut updatedCar;

}
