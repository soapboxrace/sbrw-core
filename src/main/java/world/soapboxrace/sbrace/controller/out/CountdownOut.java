package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CountdownOut {

    @JsonProperty("EventId")
    private Integer EventId;

    @JsonProperty("IsWaiting")
    private Boolean IsWaiting;

    @JsonProperty("LobbyCountdownInMilliseconds")
    private Long LobbyCountdownInMilliseconds;

    @JsonProperty("LobbyId")
    private Long LobbyId;

    @JsonProperty("LobbyStuckDurationInMilliseconds")
    private Integer LobbyStuckDurationInMilliseconds;

}
