package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CustomCarOut {

    @JsonProperty("BaseCar")
    private Integer BaseCar;

    @JsonProperty("CarClassHash")
    private Integer CarClassHash;

    @JsonProperty("Id")
    private Long Id;

    @JsonProperty("IsPreset")
    private Boolean IsPreset;

    @JsonProperty("Level")
    private Integer Level;

    @JsonProperty("Name")
    private Object Name;

    @JsonProperty("Paints")
    private List<PaintsOut> Paints;

    @JsonProperty("PerformanceParts")
    private List<PerformancePartsOut> PerformanceParts;

    @JsonProperty("PhysicsProfileHash")
    private Integer PhysicsProfileHash;

    @JsonProperty("Rating")
    private Integer Rating;

    @JsonProperty("ResalePrice")
    private Float ResalePrice;

    @JsonProperty("RideHeightDrop")
    private Float RideHeightDrop;

    @JsonProperty("SkillModParts")
    private List<SkillModPartsOut> SkillModParts;

    @JsonProperty("SkillModSlotCount")
    private Integer SkillModSlotCount;

    @JsonProperty("Version")
    private Integer Version;

    @JsonProperty("Vinyls")
    private List<VinylsOut> Vinyls;

    @JsonProperty("VisualParts")
    private List<VisualPartsOut> VisualParts;

}
