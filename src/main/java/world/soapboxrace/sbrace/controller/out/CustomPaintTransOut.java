package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public
class CustomPaintTransOut {

    @JsonProperty("Group")
    private Integer Group;

    @JsonProperty("Hue")
    private Integer Hue;

    @JsonProperty("Sat")
    private Integer Sat;

    @JsonProperty("Slot")
    private Integer Slot;

    @JsonProperty("Var")
    private Integer Var;
}
