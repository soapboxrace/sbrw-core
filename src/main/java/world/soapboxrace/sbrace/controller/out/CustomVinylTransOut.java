package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomVinylTransOut {

    @JsonProperty("Hash")
    private Integer Hash;

    @JsonProperty("Hue1")
    private Integer Hue1;

    @JsonProperty("Hue2")
    private Integer Hue2;

    @JsonProperty("Hue3")
    private Integer Hue3;

    @JsonProperty("Hue4")
    private Integer Hue4;

    @JsonProperty("Layer")
    private Integer Layer;

    @JsonProperty("Mir")
    private Boolean Mir;

    @JsonProperty("Rot")
    private Integer Rot;

    @JsonProperty("Sat1")
    private Integer Sat1;

    @JsonProperty("Sat2")
    private Integer Sat2;

    @JsonProperty("Sat3")
    private Integer Sat3;

    @JsonProperty("Sat4")
    private Integer Sat4;

    @JsonProperty("ScaleX")
    private Integer ScaleX;

    @JsonProperty("ScaleY")
    private Integer ScaleY;

    @JsonProperty("Shear")
    private Integer Shear;

    @JsonProperty("TranX")
    private Integer TranX;

    @JsonProperty("TranY")
    private Integer TranY;

    @JsonProperty("Var1")
    private Integer Var1;

    @JsonProperty("Var2")
    private Integer Var2;

    @JsonProperty("Var3")
    private Integer Var3;

    @JsonProperty("Var4")
    private Integer Var4;
}
