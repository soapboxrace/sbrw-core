package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EngagePointOut {

    @JsonProperty("X")
    Integer x = 0;

    @JsonProperty("Y")
    Integer y = 0;

    @JsonProperty("Z")
    Integer z = 0;

}
