package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EntrantResultOut {

    @JsonProperty("EventDurationInMilliseconds")
    private Long EventDurationInMilliseconds;

    @JsonProperty("EventSessionId")
    private Long EventSessionId;

    @JsonProperty("FinishReason")
    private Integer FinishReason;

    @JsonProperty("PersonaId")
    private Long PersonaId;

    @JsonProperty("Ranking")
    private Integer Ranking;

    @JsonProperty("TopSpeed")
    private Float TopSpeed;

    @JsonProperty("BestLapDurationInMilliseconds")
    private Long BestLapDurationInMilliseconds;

}
