package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventDefinitionOut {

    @JsonProperty("EngagePoint")
    private EngagePointOut engagePoint;

    @JsonProperty("CarClassHash")
    private Integer CarClassHash;

    @JsonProperty("Coins")
    private Integer Coins;

    @JsonProperty("EventId")
    private Integer EventId;

    @JsonProperty("EventLocalization")
    private Integer EventLocalization;

    @JsonProperty("EventModeDescriptionLocalization")
    private Integer EventModeDescriptionLocalization;

    @JsonProperty("EventModeIcon")
    private String EventModeIcon;

    @JsonProperty("EventModeId")
    private Integer EventModeId;

    @JsonProperty("EventModeLocalization")
    private Integer EventModeLocalization;

    @JsonProperty("IsEnabled")
    private Boolean IsEnabled;

    @JsonProperty("IsLocked")
    private Boolean IsLocked;

    @JsonProperty("Laps")
    private Integer Laps;

    @JsonProperty("Length")
    private Integer Length;

    @JsonProperty("MaxClassRating")
    private Integer MaxClassRating;

    @JsonProperty("MaxEntrants")
    private Integer MaxEntrants;

    @JsonProperty("MaxLevel")
    private Integer MaxLevel;

    @JsonProperty("MinClassRating")
    private Integer MinClassRating;

    @JsonProperty("MinEntrants")
    private Integer MinEntrants;

    @JsonProperty("MinLevel")
    private Integer MinLevel;

    @JsonProperty("RegionLocalization")
    private Integer RegionLocalization;

    @JsonProperty("RewardModes")
    private String RewardModes;

    @JsonProperty("TimeLimit")
    private Integer TimeLimit;

    @JsonProperty("TrackLayoutMap")
    private String TrackLayoutMap;

    @JsonProperty("TrackLocalization")
    private Integer TrackLocalization;
}
