package world.soapboxrace.sbrace.controller.out;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FraudConfigOut {

    private Integer enabledBitField;
    private Integer gameFileFreq;
    private Integer moduleFreq;
    private Integer startUpFreq;
    private Long userID;

}
