package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HeartBeatOut {

    @JsonProperty("MetagameFlags")
    private Integer metagameFlags = 2;
    private Integer enabledBitField = 0;
    
}
