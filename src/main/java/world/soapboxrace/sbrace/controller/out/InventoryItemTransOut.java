package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InventoryItemTransOut {

    @JsonProperty("EntitlementTag")
    private String EntitlementTag;

    @JsonProperty("Hash")
    private Integer Hash;

    @JsonProperty("InventoryId")
    private Long InventoryId;

    @JsonProperty("ProductId")
    private String ProductId;

    @JsonProperty("RemainingUseCount")
    private Integer RemainingUseCount;

    @JsonProperty("ResellPrice")
    private Float ResellPrice;

    @JsonProperty("Status")
    private String Status;

    @JsonProperty("StringHash")
    private String StringHash;

    @JsonProperty("VirtualItemType")
    private String VirtualItemType;
}
