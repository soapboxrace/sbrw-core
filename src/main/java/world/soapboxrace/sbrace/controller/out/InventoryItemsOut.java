package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InventoryItemsOut {

    @JsonProperty("InventoryItemTrans")
    private InventoryItemTransOut inventoryItemTrans;

}
