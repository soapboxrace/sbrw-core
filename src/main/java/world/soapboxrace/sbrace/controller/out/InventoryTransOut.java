package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class InventoryTransOut {

    @JsonProperty("InventoryItems")
    private List<InventoryItemsOut> InventoryItems ;

    @JsonProperty("PerformancePartsCapacity")
    private Integer PerformancePartsCapacity;

    @JsonProperty("PerformancePartsUsedSlotCount")
    private Integer PerformancePartsUsedSlotCount;

    @JsonProperty("SkillModPartsCapacity")
    private Integer SkillModPartsCapacity;

    @JsonProperty("SkillModPartsUsedSlotCount")
    private Integer SkillModPartsUsedSlotCount;

    @JsonProperty("VisualPartsCapacity")
    private Integer VisualPartsCapacity;

    @JsonProperty("VisualPartsUsedSlotCount")
    private Integer VisualPartsUsedSlotCount;
}
