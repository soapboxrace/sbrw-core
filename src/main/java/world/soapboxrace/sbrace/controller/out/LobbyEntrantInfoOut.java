package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LobbyEntrantInfoOut {

    @JsonProperty("GridIndex")
    private Integer gridIndex;

    @JsonProperty("Heat")
    private Float heat;

    @JsonProperty("Level")
    private Integer level;

    @JsonProperty("PersonaId")
    private Long personaId;

    @JsonProperty("State")
    private String state;

}
