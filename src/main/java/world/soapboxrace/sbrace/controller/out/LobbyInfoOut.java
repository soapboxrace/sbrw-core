package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class LobbyInfoOut {

    @JsonProperty("Countdown")
    private CountdownOut Countdown;

    @JsonProperty("Entrants")
    private List<EntrantsOut> entrants;

    @JsonProperty("EventId")
    private Integer EventId;

    @JsonProperty("IsInviteEnabled")
    private Boolean IsInviteEnabled;

    @JsonProperty("LobbyId")
    private Long LobbyId;

    @JsonProperty("LobbyInviteId")
    private Long LobbyInviteId;


}
