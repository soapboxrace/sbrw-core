package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LuckyDrawInfoOut {

    @JsonProperty("CurrentStreak")
    private Integer CurrentStreak = 0;

    @JsonProperty("IsStreakBroken")
    private Boolean IsStreakBroken = false;

    @JsonProperty("NumBoxAnimations")
    private Integer NumBoxAnimations = 0;

    @JsonProperty("NumCards")
    private Integer NumCards = 0;

}
