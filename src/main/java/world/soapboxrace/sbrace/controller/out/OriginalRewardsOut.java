package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OriginalRewardsOut {

    @JsonProperty("Rep")
    private Integer rep = 0;

    @JsonProperty("Tokens")
    private Integer tokens = 0;

}
