package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class OwnedCarTransOut {

    @JsonProperty("CustomCar")
    private CustomCarOut CustomCar;

    @JsonProperty("Durability")
    private Integer Durability;

    @JsonProperty("Heat")
    private Float Heat;

    @JsonProperty("Id")
    private Long Id;

    @JsonProperty("OwnershipType")
    private String OwnershipType;
}
