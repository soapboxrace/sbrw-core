package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonaBaseOut {

    @JsonProperty("Badges")
    private String Badges;

    @JsonProperty("IconIndex")
    private Integer IconIndex;

    @JsonProperty("Level")
    private Integer Level;

    @JsonProperty("Name")
    private String Name;

    @JsonProperty("PersonaId")
    private Long PersonaId;

    @JsonProperty("Presence")
    private Integer Presence;

    @JsonProperty("Score")
    private Integer Score;

    @JsonProperty("UserId")
    private Long UserId;
}
