package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonaOut {

    @JsonProperty("Boost")
    private Float Boost;

    @JsonProperty("Cash")
    private Float Cash;

    @JsonProperty("IconIndex")
    private Integer IconIndex;

    @JsonProperty("Level")
    private Integer Level;

    @JsonProperty("Name")
    private String Name;

    @JsonProperty("PercentToLevel")
    private Float PercentToLevel;

    @JsonProperty("PersonaId")
    private Long PersonaId;

    @JsonProperty("Rating")
    private Float Rating;

    @JsonProperty("Rep")
    private Float Rep;

    @JsonProperty("RepAtCurrentLevel")
    private Integer RepAtCurrentLevel;

    @JsonProperty("Score")
    private Integer Score;

}
