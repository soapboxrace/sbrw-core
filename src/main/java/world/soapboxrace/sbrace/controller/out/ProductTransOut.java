package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductTransOut {

    @JsonProperty("BundleItems")
    String bundleItems;

    @JsonProperty("CategoryId")
    String categoryId;

    @JsonProperty("Currency")
    String currency;

    @JsonProperty("Description")
    String description;

    @JsonProperty("DurationMinute")
    Integer durationMinute;

    @JsonProperty("Hash")
    Integer hash;

    @JsonProperty("Icon")
    String icon;

    @JsonProperty("Level")
    Integer level;

    @JsonProperty("Price")
    Float price;

    @JsonProperty("LongDescription")
    String longDescription;

    @JsonProperty("Priority")
    Integer priority;

    @JsonProperty("ProductId")
    String productId;

    @JsonProperty("SecondaryIcon")
    String secondaryIcon;

    @JsonProperty("UseCount")
    Integer useCount;

    @JsonProperty("WebIcon")
    String webIcon;

    @JsonProperty("WebLocation")
    String webLocation;

    @JsonProperty("ProductTitle")
    String productTitle;

    @JsonProperty("ProductType")
    String productType;

    @JsonProperty("VisualStyle")
    private String visualStyle;

}
