package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ProfileDataOut {

    @JsonProperty("Boost")
    private Float boost;

    @JsonProperty("Cash")
    private Float cash;

    @JsonProperty("IconIndex")
    private Integer iconIndex;

    @JsonProperty("Level")
    private Integer level;

    @JsonProperty("Name")
    private String name;

    @JsonProperty("PercentToLevel")
    private Float percentToLevel;

    @JsonProperty("PersonaId")
    private Long personaId;

    @JsonProperty("Rating")
    private Float rating;

    @JsonProperty("Rep")
    private Float rep;

    @JsonProperty("RepAtCurrentLevel")
    private Integer repAtCurrentLevel;

    @JsonProperty("Score")
    private Integer score;

}
