package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegionInfoOut {

    @JsonProperty("CountdownProposalInMilliseconds")
    private Integer CountdownProposalInMilliseconds;

    @JsonProperty("DirectConnectTimeoutInMilliseconds")
    private Integer DirectConnectTimeoutInMilliseconds;

    @JsonProperty("DropOutTimeInMilliseconds")
    private Integer DropOutTimeInMilliseconds;

    @JsonProperty("EventLoadTimeoutInMilliseconds")
    private Integer EventLoadTimeoutInMilliseconds;

    @JsonProperty("HeartbeatIntervalInMilliseconds")
    private Integer HeartbeatIntervalInMilliseconds;

    @JsonProperty("UdpRelayBandwidthInBps")
    private Integer UdpRelayBandwidthInBps;

    @JsonProperty("UdpRelayTimeoutInMilliseconds")
    private Integer UdpRelayTimeoutInMilliseconds;

}
