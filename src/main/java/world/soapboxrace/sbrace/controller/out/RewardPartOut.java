package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RewardPartOut {

    @JsonProperty("RepPart")
    private Integer RepPart = 0;

    @JsonProperty("RewardCategory")
    private String RewardCategory;

    @JsonProperty("RewardType")
    private String RewardType;

    @JsonProperty("TokenPart")
    private Integer TokenPart = 0;
}
