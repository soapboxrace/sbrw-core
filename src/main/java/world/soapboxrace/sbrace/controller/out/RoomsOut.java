package world.soapboxrace.sbrace.controller.out;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RoomsOut {

    private List<ChatRoomOut> chatRoom = List.of(new ChatRoomOut());

}
