package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SocialSettingsOut {

    @JsonProperty("AppearOffline")
    private Boolean AppearOffline;

    @JsonProperty("DeclineGroupInvite")
    private Integer DeclineGroupInvite;

    @JsonProperty("DeclineIncommingFriendRequests")
    private Boolean DeclineIncommingFriendRequests;

    @JsonProperty("DeclinePrivateInvite")
    private Integer DeclinePrivateInvite;

    @JsonProperty("HideOfflineFriends")
    private Boolean HideOfflineFriends;

    @JsonProperty("ShowNewsOnSignIn")
    private Boolean ShowNewsOnSignIn;

    @JsonProperty("ShowOnlyPlayersInSameChatChannel")
    private Boolean ShowOnlyPlayersInSameChatChannel;
}
