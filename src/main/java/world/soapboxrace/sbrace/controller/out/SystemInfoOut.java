package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class SystemInfoOut {

    @JsonProperty("ClientVersionCheck")
    private Boolean clientVersionCheck;

    @JsonProperty("EntitlementsToDownload")
    private Boolean entitlementsToDownload;

    @JsonProperty("ForcePermanentSession")
    private Boolean forcePermanentSession;

    @JsonProperty("PersonaCacheTimeout")
    private Integer personaCacheTimeout;

    @JsonProperty("Time")
    //2022-12-11T07:02:29.365-04:00
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'-04:00'")
    private LocalDateTime time;

}
