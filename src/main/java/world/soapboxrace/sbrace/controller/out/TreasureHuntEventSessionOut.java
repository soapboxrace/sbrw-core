package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TreasureHuntEventSessionOut {

    @JsonProperty("CoinsCollected")
    private Integer CoinsCollected;

    @JsonProperty("IsStreakBroken")
    private Boolean IsStreakBroken;

    @JsonProperty("NumCoins")
    private Integer NumCoins;

    @JsonProperty("Seed")
    private Integer Seed;

    @JsonProperty("Streak")
    private Integer Streak;

}
