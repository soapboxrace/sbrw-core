package world.soapboxrace.sbrace.controller.out;

import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.wrapper.PersonaWrapper;

import java.util.List;

@Getter
@Setter
public class UserInfoOut {

    private List<PersonaWrapper> personas;
    private UserOut user;
    private Integer defaultPersonaIdx;

}
