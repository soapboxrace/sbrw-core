package world.soapboxrace.sbrace.controller.out;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserOut {

    private Boolean fullGameAccess;
    private Boolean isComplete;
    private String remoteUserId;
    private String securityToken;
    private Boolean subscribeMsg;
    private Long userId;

}
