package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserSettingsOut {

    @JsonProperty("CarCacheAgeLimit")
    private Integer CarCacheAgeLimit;

    @JsonProperty("IsRaceNowEnabled")
    private Boolean IsRaceNowEnabled;

    @JsonProperty("MaxCarCacheSize")
    private Integer MaxCarCacheSize;

    @JsonProperty("MinRaceNowLevel")
    private Integer MinRaceNowLevel;

    @JsonProperty("VoipAvailable")
    private Boolean VoipAvailable;

    private Boolean firstTimeLogin;

    private Integer maxLevel;
    private Boolean starterPackApplied;
    private Long userId;

}
