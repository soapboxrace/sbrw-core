package world.soapboxrace.sbrace.controller.out;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public
class WalletTransOut {

    @JsonProperty("Balance")
    private Float Balance;

    @JsonProperty("Currency")
    private String Currency;

}
