package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthenticateUserWrapper {

    @JsonProperty("LoginStatusVO")
    private LoginStatusVO loginStatusVO;

    @Getter
    @Setter
    public static class LoginStatusVO {
        @JsonProperty("UserId")
        private Long userId;

        @JsonProperty("LoginToken")
        private String loginToken;

        @JsonProperty("Description")
        private String description;
    }

}
