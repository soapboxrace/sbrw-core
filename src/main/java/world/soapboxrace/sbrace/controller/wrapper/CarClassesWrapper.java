package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.ArrayOfCarClassOut;

import java.util.List;

@Getter
@Setter
public class CarClassesWrapper {

    @JsonProperty("ArrayOfCarClass")
    private List<ArrayOfCarClassOut> arrayOfCarClass;

}
