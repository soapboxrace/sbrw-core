package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.ArrayOfProductTransOut;
import world.soapboxrace.sbrace.controller.out.ProductTransOutWrapper;

import java.util.List;

@Getter
@Setter
public class CatalogProductsInCategoryWrapper {

    @JsonProperty("ArrayOfProductTrans")
    private List<ProductTransOutWrapper> productTrans;

}
