package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.in.OwnedCarTransIn;

@Getter
@Setter
public class CommerceSessionTransIn {

    @JsonProperty("UpdatedCar")
    private OwnedCarTransIn UpdatedCar;

}
