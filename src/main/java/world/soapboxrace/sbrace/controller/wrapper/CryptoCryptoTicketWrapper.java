package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.ClientServerCryptoTicketOut;

@Getter
@Setter
public class CryptoCryptoTicketWrapper {

    @JsonProperty("ClientServerCryptoTicket")
    private ClientServerCryptoTicketOut clientServerCryptoTicketOut;

}
