package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.UdpRelayCryptoTicketOut;

@Getter
@Setter
public class CryptoRelayCryptoTicketWrapper {

    @JsonProperty("UdpRelayCryptoTicket")
    private UdpRelayCryptoTicketOut udpRelayCryptoTicket;

}
