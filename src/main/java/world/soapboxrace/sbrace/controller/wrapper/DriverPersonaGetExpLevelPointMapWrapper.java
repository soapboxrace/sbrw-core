package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.ArrayOfIntOut;

@Getter
@Setter
public class DriverPersonaGetExpLevelPointMapWrapper {

    @JsonProperty("ArrayOfInt")
    private ArrayOfIntOut arrayOfInt;

}
