package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.ArrayOfPersonaBaseOut;

@Getter
@Setter
public class DriverPersonaGetPersonaBaseFromListWrapper {

    @JsonProperty("ArrayOfPersonaBase")
    private ArrayOfPersonaBaseOut arrayOfPersonaBase;

}
