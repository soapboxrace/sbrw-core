package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.in.PersonaIdArrayIn;

import java.util.List;

@Getter
@Setter
public class DriverPersonaGetPersonaBaseFromListWrapperIn {

    @JsonProperty("PersonaIdArray")
    private PersonaIdArrayIn PersonaIdArray;

}
