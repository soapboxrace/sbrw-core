package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.ProfileDataOut;

@Getter
@Setter
public class DriverPersonaGetPersonaInfoWrapper {

    @JsonProperty("ProfileData")
    private ProfileDataOut profileData;

}
