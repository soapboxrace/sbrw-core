package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.ArrayOfStringOut;

@Getter
@Setter
public class DriverPersonaReserveNameWrapper {

    @JsonProperty("ArrayOfString")
    private ArrayOfStringOut arrayOfStringOut;

}
