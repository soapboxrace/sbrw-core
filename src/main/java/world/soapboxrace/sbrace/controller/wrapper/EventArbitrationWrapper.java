package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.DragEventResultOut;
import world.soapboxrace.sbrace.controller.out.PursuitEventResultOut;
import world.soapboxrace.sbrace.controller.out.RouteEventResultOut;
import world.soapboxrace.sbrace.controller.out.TeamEscapeEventResultOut;

@Getter
@Setter
public class EventArbitrationWrapper {

    @JsonProperty("DragEventResult")
    private DragEventResultOut dragEventResult;

    @JsonProperty("PursuitEventResult")
    private PursuitEventResultOut pursuitEventResult;

    @JsonProperty("RouteEventResult")
    private RouteEventResultOut routeEventResult;

    @JsonProperty("TeamEscapeEventResult")
    private TeamEscapeEventResultOut teamEscapeEventResult;

}
