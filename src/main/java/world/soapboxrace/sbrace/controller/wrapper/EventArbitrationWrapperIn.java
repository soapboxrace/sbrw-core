package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.in.DragArbitrationPacketIn;
import world.soapboxrace.sbrace.controller.in.PursuitArbitrationPacketIn;
import world.soapboxrace.sbrace.controller.in.RouteArbitrationPacketIn;
import world.soapboxrace.sbrace.controller.in.TeamEscapeArbitrationPacketIn;

@Getter
@Setter
public class EventArbitrationWrapperIn {

    @JsonProperty("DragArbitrationPacket")
    private DragArbitrationPacketIn dragArbitrationPacket;

    @JsonProperty("RouteArbitrationPacket")
    private RouteArbitrationPacketIn routeArbitrationPacket;

    @JsonProperty("PursuitArbitrationPacket")
    private PursuitArbitrationPacketIn pursuitArbitrationPacket;

    @JsonProperty("TeamEscapeArbitrationPacket")
    private TeamEscapeArbitrationPacketIn teamEscapeArbitrationPacket;

}
