package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.EventsOut;
import world.soapboxrace.sbrace.controller.out.EventsPacketOut;

import java.util.List;

@Getter
@Setter
public class EventsAvaliableAtLevelWrapper {

    @JsonProperty("EventsPacket")
    private EventsPacketOut EventsPacket;

}
