package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.TreasureHuntEventSessionOut;

@Getter
@Setter
public class EventsGetTreasureHuntEventSessionWrapper {

    @JsonProperty("TreasureHuntEventSession")
    private TreasureHuntEventSessionOut treasureHuntEventSession;

}
