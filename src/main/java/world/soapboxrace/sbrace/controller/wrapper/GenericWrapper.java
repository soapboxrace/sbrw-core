package world.soapboxrace.sbrace.controller.wrapper;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenericWrapper {

    private String nothing = "";
    
}
