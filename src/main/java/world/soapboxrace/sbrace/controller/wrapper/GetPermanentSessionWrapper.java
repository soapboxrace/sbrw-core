package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.UserInfoOut;

@Getter
@Setter
public class GetPermanentSessionWrapper {

    @JsonProperty("UserInfo")
    private UserInfoOut userInfo;

}
