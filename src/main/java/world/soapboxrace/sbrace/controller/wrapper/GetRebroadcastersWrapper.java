package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.ArrayOfUdpRelayInfoOut;

@Getter
@Setter
public class GetRebroadcastersWrapper {

    @JsonProperty("ArrayOfUdpRelayInfo")
    private ArrayOfUdpRelayInfoOut arrayOfUdpRelayInfo;

}
