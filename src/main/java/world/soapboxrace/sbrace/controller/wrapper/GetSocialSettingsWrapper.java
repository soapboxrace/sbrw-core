package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.SocialSettingsOut;

@Getter
@Setter
public class GetSocialSettingsWrapper {

    @JsonProperty("SocialSettings")
    private SocialSettingsOut socialSettings;

}
