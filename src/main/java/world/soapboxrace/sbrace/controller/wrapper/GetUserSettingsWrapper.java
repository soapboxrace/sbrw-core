package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.UserSettingsOut;

@Getter
@Setter
public class GetUserSettingsWrapper {

    @JsonProperty("User_Settings")
    private UserSettingsOut userSettings;
}
