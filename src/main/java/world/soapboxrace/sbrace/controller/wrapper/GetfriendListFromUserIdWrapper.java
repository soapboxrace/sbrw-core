package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.PersonaFriendsListOut;

@Getter
@Setter
public class GetfriendListFromUserIdWrapper {

    @JsonProperty("PersonaFriendsList")
    private PersonaFriendsListOut personaFriendsList;

}
