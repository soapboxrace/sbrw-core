package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.LoginAnnouncementsDefinitionOut;

@Getter
@Setter
public class LoginAnnouncementsWrapper {

    @JsonProperty("LoginAnnouncementsDefinition")
    private LoginAnnouncementsDefinitionOut loginAnnouncementsDefinition;

}
