package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.LobbyInfoOut;

@Getter
@Setter
public class MatchmakingAcceptinviteWrapper {

    @JsonProperty("LobbyInfo")
    private LobbyInfoOut lobbyInfo;

}
