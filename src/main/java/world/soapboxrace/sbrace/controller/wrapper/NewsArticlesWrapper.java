package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.ArrayOfNewsArticleTransOut;

@Getter
@Setter
public class NewsArticlesWrapper {

    @JsonProperty("ArrayOfNewsArticleTrans")
    private ArrayOfNewsArticleTransOut arrayOfNewsArticleTrans;

}
