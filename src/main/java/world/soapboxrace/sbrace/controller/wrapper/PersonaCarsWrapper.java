package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.CarsOwnedByPersonaOut;
import world.soapboxrace.sbrace.controller.out.OwnedCarTransOut;

import java.util.List;

@Getter
@Setter
public class PersonaCarsWrapper {

    @JsonProperty("ArrayOfOwnedCarTrans")
    private List<CarsOwnedByPersonaOut> ArrayOfOwnedCarTrans;

}
