package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonaCommerceWrapperIn {

    @JsonProperty("CommerceSessionTrans")
    private CommerceSessionTransIn commerceSessionTrans;
    
}
