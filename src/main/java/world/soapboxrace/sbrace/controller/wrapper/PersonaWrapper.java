package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.PersonaOut;

@Getter
@Setter
public class PersonaWrapper {

    @JsonProperty("ProfileData")
    private PersonaOut persona;

}
