package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.in.BasketTransIn;

@Getter
@Setter
public class PersonasBasketsWrapperIn {

    @JsonProperty("BasketTrans")
    private BasketTransIn BasketTrans;
    
}
