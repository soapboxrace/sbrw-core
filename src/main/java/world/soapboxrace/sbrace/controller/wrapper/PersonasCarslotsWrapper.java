package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.CarSlotInfoTransOut;

@Getter
@Setter
public class PersonasCarslotsWrapper {

    @JsonProperty("CarSlotInfoTrans")
    private CarSlotInfoTransOut carSlotInfoTrans;
}
