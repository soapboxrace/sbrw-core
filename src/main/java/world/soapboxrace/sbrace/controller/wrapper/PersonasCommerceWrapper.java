package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.CommerceResultTransOut;

@Getter
@Setter
public class PersonasCommerceWrapper {

    @JsonProperty("CommerceSessionResultTrans")
    private CommerceResultTransOut CommerceResultTrans;

}
