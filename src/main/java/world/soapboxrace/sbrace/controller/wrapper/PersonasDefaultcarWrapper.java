package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.OwnedCarTransOut;

@Getter
@Setter
public class PersonasDefaultcarWrapper {

    @JsonProperty("OwnedCarTrans")
    private OwnedCarTransOut ownedCarTrans;

}
