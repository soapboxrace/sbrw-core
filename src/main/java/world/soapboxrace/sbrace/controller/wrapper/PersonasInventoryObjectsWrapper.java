package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.InventoryTransOut;

@Getter
@Setter
public class PersonasInventoryObjectsWrapper {

    @JsonProperty("InventoryTrans")
    private InventoryTransOut inventoryTrans;

}
