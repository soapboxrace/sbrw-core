package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.FraudConfigOut;

@Getter
@Setter
public class SecurityFraudConfigWrapper {

    @JsonProperty("FraudConfig")
    private FraudConfigOut fraudConfig;

}
