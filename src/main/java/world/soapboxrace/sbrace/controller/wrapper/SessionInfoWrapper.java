package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.SessionInfoOut;

@Getter
@Setter
public class SessionInfoWrapper {

    @JsonProperty("SessionInfo")
    private SessionInfoOut sessionInfo;

}
