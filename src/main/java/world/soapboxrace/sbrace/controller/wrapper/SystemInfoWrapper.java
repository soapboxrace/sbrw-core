package world.soapboxrace.sbrace.controller.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import world.soapboxrace.sbrace.controller.out.SystemInfoOut;

@Getter
@Setter
public class SystemInfoWrapper {

    @JsonProperty("SystemInfo")
    private SystemInfoOut systemInfoOut;

}
