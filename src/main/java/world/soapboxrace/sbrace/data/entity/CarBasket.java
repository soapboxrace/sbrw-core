package world.soapboxrace.sbrace.data.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "CAR_BASKET")
public class CarBasket {

    @Id
    private String id;

    @Column(columnDefinition = "TEXT")
    private String json;

}
