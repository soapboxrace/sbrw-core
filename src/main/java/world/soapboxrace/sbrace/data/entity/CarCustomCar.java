package world.soapboxrace.sbrace.data.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "CAR_CUSTOM_CAR")
public class CarCustomCar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    private Integer BaseCar;

    private Integer CarClassHash;

    private Boolean IsPreset;

    private Integer Level;

    private String Name;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_CAR_CUSTOM_CAR", foreignKey = @ForeignKey(name = "FK_PAINT_CUSTOMCAR"))
    private List<CarPaint> Paints;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_CAR_CUSTOM_CAR", foreignKey = @ForeignKey(name = "FK_PERF_CUSTOMCAR"))
    private List<CarPerformancePart> PerformanceParts;

    private Integer PhysicsProfileHash;

    private Integer Rating;

    private Float ResalePrice;

    private Float RideHeightDrop;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_CAR_CUSTOM_CAR", foreignKey = @ForeignKey(name = "FK_SKILL_CUSTOMCAR"))
    private List<CarSkillModPart> SkillModParts;

    private Integer SkillModSlotCount;

    private Integer Version;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_CAR_CUSTOM_CAR", foreignKey = @ForeignKey(name = "FK_VINYL_CUSTOMCAR"))
    private List<CarVinyl> Vinyls;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_CAR_CUSTOM_CAR", foreignKey = @ForeignKey(name = "FK_VISUAL_CUSTOMCAR"))
    private List<CarVisualPart> VisualParts;

}
