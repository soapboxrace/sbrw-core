package world.soapboxrace.sbrace.data.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "CAR_OWNED_CAR")
public class CarOwnedCar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ID_SB_PERSONA", foreignKey = @ForeignKey(name = "FK_CAR_PERSONA"))
    private SbPersona persona;

    private Boolean defaultCar;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_CAR_CUSTOM_CAR", foreignKey = @ForeignKey(name = "FK_CUSTOMCAR_OWNED"))
    private CarCustomCar customCar;

    private Integer durability;

    private Float heat;

    private String ownershipType;

    @Column(columnDefinition = "TEXT")
    private String customCarJson;

}
