package world.soapboxrace.sbrace.data.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "CAR_PAINT")
public class CarPaint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "PAINT_GROUP")
    private Integer group;

    private Integer Hue;

    private Integer Sat;

    private Integer Slot;

    private Integer Var;

}
