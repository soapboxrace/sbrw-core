package world.soapboxrace.sbrace.data.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "CAR_PERFORMANCE_PART")
public class CarPerformancePart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer PerformancePartAttribHash;

}
