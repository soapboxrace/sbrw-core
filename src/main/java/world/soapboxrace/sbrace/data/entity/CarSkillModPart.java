package world.soapboxrace.sbrace.data.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "CAR_SKILL_MOD_PART")
public class CarSkillModPart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Boolean IsFixed;

    private Integer SkillModPartAttribHash;

}
