package world.soapboxrace.sbrace.data.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "CAR_VINYL")
public class CarVinyl {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer Hash;

    private Integer Hue1;

    private Integer Hue2;

    private Integer Hue3;

    private Integer Hue4;

    private Integer Layer;

    private Boolean Mir;

    private Integer Rot;

    private Integer Sat1;

    private Integer Sat2;

    private Integer Sat3;

    private Integer Sat4;

    private Integer ScaleX;

    private Integer ScaleY;

    private Integer Shear;

    private Integer TranX;

    private Integer TranY;

    private Integer Var1;

    private Integer Var2;

    private Integer Var3;

    private Integer Var4;

}
