package world.soapboxrace.sbrace.data.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "INVENTORY")
public class Inventory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "ID_SB_PERSONA", foreignKey = @ForeignKey(name = "FK_INVENTORY_PERSONA"))
    private SbPersona persona;

    private int performancePartsCapacity;

    private int performancePartsUsedSlotCount;

    private int skillModPartsCapacity;

    private int skillModPartsUsedSlotCount;

    private int visualPartsCapacity;

    private int visualPartsUsedSlotCount;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_INVENTORY", foreignKey = @ForeignKey(name = "FK_ITEM_INVENTORY"))
    private List<InventoryItem> items = new ArrayList<>();

}
