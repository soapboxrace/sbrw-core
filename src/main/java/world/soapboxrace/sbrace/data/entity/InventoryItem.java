package world.soapboxrace.sbrace.data.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "INVENTORY_ITEM")
public class InventoryItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //	@ManyToOne
//	@JoinColumn(name = "inventoryId", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_PERSINVITEM_PERSINV"))
//    private Inventory inventory;

//	@ManyToOne
//	@JoinColumn(name = "personaId")
//	private PersonaEntity persona;

    private String entitlementTag;

    private String expirationDate;

    private Integer hash;

    private String productId;

    private int remainingUseCount;

    private float resalePrice;

    private String status;

    private String stringHash;

    private String virtualItemType;

}
