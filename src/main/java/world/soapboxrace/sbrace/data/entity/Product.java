package world.soapboxrace.sbrace.data.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "PRODUCT")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "ID_PRODUCT_CATEGORY", foreignKey = @ForeignKey(name = "FK_PRODUCT_CATEGORY"))
	private ProductCategory category;

	private String currency;
	private String description;
	private Integer durationMinute;
	private Integer hash;
	private String icon;
	private Integer level;
	private String longDescription;
	private Float price;
	private Float resalePrice;
	private Integer priority;
	private String productId;
	private String productTitle;
	private String productType;
	private Integer useCount;
	private String webIcon;
	private String categoryName;
	private Boolean enabled;
	private Integer minLevel;
	private Boolean premium = false;
	private Boolean isDropable;
	private Integer topSpeed = 0;
	private Integer accel = 0;
	private Integer handling = 0;
	private Float skillValue;

}
