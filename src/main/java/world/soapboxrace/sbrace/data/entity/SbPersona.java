package world.soapboxrace.sbrace.data.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "SBPERSONA")
public class SbPersona {

    @Id
    @GeneratedValue(generator = "sbpersona_id_seq")
    @SequenceGenerator(name = "sbpersona_id_seq", initialValue = 100)
    private long id;
    private float boost;
    private float cash;
    private int iconIndex;
    private int level;
    private String motto;
    private String name;
    private float percentToLevel;
    private float rating;
    private float rep;
    private int repAtCurrentLevel;
    private int score;
    private long userId;

    public long getPersonaId() {
        return id;
    }

    public void setPersonaId(long id) {
        this.id = id;
    }
}