package world.soapboxrace.sbrace.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import world.soapboxrace.sbrace.data.entity.CarBasket;

public interface CarBasketRepository extends JpaRepository<CarBasket, String> {

}
