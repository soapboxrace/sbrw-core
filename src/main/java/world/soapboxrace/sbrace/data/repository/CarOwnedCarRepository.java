package world.soapboxrace.sbrace.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import world.soapboxrace.sbrace.data.entity.CarOwnedCar;
import world.soapboxrace.sbrace.data.entity.SbPersona;

import java.util.List;

public interface CarOwnedCarRepository extends JpaRepository<CarOwnedCar, Long> {

    List<CarOwnedCar> getCarOwnedCarsByPersonaOrderByDefaultCarDesc(SbPersona persona);

    CarOwnedCar getCarOwnedCarByPersonaAndDefaultCarIsTrue(SbPersona persona);

    @Modifying
    @Query("UPDATE CarOwnedCar car set car.defaultCar=false where car.persona.id = :personaId")
    void resetDefaultCar(@Param("personaId") Long personaId);

    void deleteAllByPersona(SbPersona persona);

}
