package world.soapboxrace.sbrace.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import world.soapboxrace.sbrace.data.entity.Event;

public interface EventRepository extends JpaRepository<Event, Long> {
}
