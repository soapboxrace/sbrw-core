package world.soapboxrace.sbrace.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import world.soapboxrace.sbrace.data.entity.Inventory;
import world.soapboxrace.sbrace.data.entity.SbPersona;

import java.util.Optional;

public interface InventoryRepository extends JpaRepository<Inventory, Long> {

    Optional<Inventory> getInventoryByPersona_Id(Long personaId);

    void deleteAllByPersona(SbPersona persona);

}
