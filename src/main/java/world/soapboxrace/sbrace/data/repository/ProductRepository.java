package world.soapboxrace.sbrace.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import world.soapboxrace.sbrace.data.entity.Product;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findProductByEnabledAndCategoryNameAndProductType(Boolean enabled,
                                                                    String categoryName,
                                                                    String productType);

    Product findProductByProductId(String productId);

}
