package world.soapboxrace.sbrace.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import world.soapboxrace.sbrace.data.entity.SbPersona;

import java.util.List;

public interface SbPersonaRepository extends JpaRepository<SbPersona, Long> {

    List<SbPersona> findByUserId(Long userId);

}
