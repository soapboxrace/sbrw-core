package world.soapboxrace.sbrace.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import world.soapboxrace.sbrace.data.entity.SbUser;

public interface SbUserRepository extends JpaRepository<SbUser, Long> {

    SbUser findByEmail(String email);

}
