package world.soapboxrace.sbrace.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import world.soapboxrace.sbrace.component.ObjectMapperStatic;
import world.soapboxrace.sbrace.data.entity.*;
import world.soapboxrace.sbrace.data.repository.CarBasketRepository;
import world.soapboxrace.sbrace.data.repository.CarOwnedCarRepository;
import world.soapboxrace.sbrace.data.repository.SbPersonaRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CarService {

    private final SbPersonaRepository sbPersonaRepository;

    private final CarBasketRepository carBasketRepository;

    private final CarOwnedCarRepository carOwnedCarRepository;

    private final ObjectMapper objectMapper = ObjectMapperStatic.get();

    @Transactional
    public void buyCar(Long personaId, Product product) {
        Optional<CarBasket> carBasketOptional = carBasketRepository.findById(product.getProductId());
        carBasketOptional.ifPresent(carBasket -> {
            String jsonStr = carBasket.getJson();
            try {
                CarOwnedCar carOwnedCar = objectMapper.readValue(jsonStr, CarOwnedCar.class);
                carOwnedCar.setDefaultCar(true);
                String customCarJson = objectMapper.writeValueAsString(carOwnedCar.getCustomCar());
                carOwnedCar.setCustomCarJson(customCarJson);
                carOwnedCar.setCustomCar(null);
                Optional<SbPersona> sbPersonaOptional = sbPersonaRepository.findById(personaId);
                sbPersonaOptional.ifPresent(carOwnedCar::setPersona);
                carOwnedCarRepository.resetDefaultCar(personaId);
                carOwnedCarRepository.save(carOwnedCar);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public CarOwnedCar getPersonaDefaultCar(Long personaId) {
        CarOwnedCar carOwnedCar = new CarOwnedCar();
        Optional<SbPersona> sbPersona = sbPersonaRepository.findById(personaId);
        if (sbPersona.isPresent()) {
            List<CarOwnedCar> carOwnedCarList = carOwnedCarRepository.getCarOwnedCarsByPersonaOrderByDefaultCarDesc(sbPersona.get());
            if (carOwnedCarList.isEmpty()) {
                return carOwnedCar;
            }
            carOwnedCar = carOwnedCarList.getFirst();
            String customCarJson = carOwnedCar.getCustomCarJson();
            try {
                carOwnedCar.setCustomCar(objectMapper.readValue(customCarJson, CarCustomCar.class));
                carOwnedCar.getCustomCar().setId(carOwnedCar.getId());
                carOwnedCar.setCustomCarJson(null);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }
        return carOwnedCar;
    }

    @Transactional
    public void setDefaultCar(Long personaId, Long carId) {
        carOwnedCarRepository.resetDefaultCar(personaId);
        Optional<CarOwnedCar> ownedCarOptional = carOwnedCarRepository.findById(carId);
        ownedCarOptional.ifPresent(carOwnedCar -> {
            carOwnedCar.setDefaultCar(true);
            carOwnedCarRepository.save(carOwnedCar);
        });
    }

    public List<CarOwnedCar> getPersonaCars(Long personaId) {
        Optional<SbPersona> sbPersona = sbPersonaRepository.findById(personaId);
        List<CarOwnedCar> carOwnedCarsByPersonaOrderByDefaultCarDesc = carOwnedCarRepository.getCarOwnedCarsByPersonaOrderByDefaultCarDesc(sbPersona.get());
        carOwnedCarsByPersonaOrderByDefaultCarDesc.forEach(carOwnedCar -> {
            try {
                carOwnedCar.setCustomCar(objectMapper.readValue(carOwnedCar.getCustomCarJson(), CarCustomCar.class));
                carOwnedCar.getCustomCar().setId(carOwnedCar.getId());
                carOwnedCar.setCustomCarJson(null);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        });

        return sbPersona.map(carOwnedCarRepository::getCarOwnedCarsByPersonaOrderByDefaultCarDesc).orElse(new ArrayList<>());
    }

    public CarOwnedCar saveCar(Long personaId, CarOwnedCar carOwnedCar) {
        Optional<SbPersona> sbPersona = sbPersonaRepository.findById(personaId);
        CarCustomCar customCar = carOwnedCar.getCustomCar();
        carOwnedCar.setPersona(sbPersona.get());
        carOwnedCar.setDurability(100);
        carOwnedCar.setHeat(1F);
        try {
            String customCarJson = objectMapper.writeValueAsString(customCar);
            carOwnedCar.setCustomCarJson(customCarJson);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        carOwnedCar.setCustomCar(null);
        carOwnedCarRepository.save(carOwnedCar);
        carOwnedCar.setCustomCar(customCar);
        return carOwnedCar;
    }

    public void deleteCar(Long carId) {
        carOwnedCarRepository.deleteById(carId);
    }

    public void deleteByPersona(SbPersona sbPersona) {
        carOwnedCarRepository.deleteAllByPersona(sbPersona);
    }

}
