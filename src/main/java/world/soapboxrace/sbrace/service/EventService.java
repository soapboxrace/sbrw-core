package world.soapboxrace.sbrace.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import world.soapboxrace.sbrace.data.entity.Event;
import world.soapboxrace.sbrace.data.repository.EventRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EventService {

    private final EventRepository eventRepository;

    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }

}
