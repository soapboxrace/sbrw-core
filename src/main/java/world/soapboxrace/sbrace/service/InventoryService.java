package world.soapboxrace.sbrace.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import world.soapboxrace.sbrace.data.entity.Inventory;
import world.soapboxrace.sbrace.data.entity.InventoryItem;
import world.soapboxrace.sbrace.data.entity.SbPersona;
import world.soapboxrace.sbrace.data.repository.InventoryRepository;
import world.soapboxrace.sbrace.session.TokenSessionService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class InventoryService {

    private final TokenSessionService tokenSessionService;
    private final InventoryRepository inventoryRepository;

    public void createInventory(SbPersona sbPersona) {
        Inventory inventory = new Inventory();
        inventory.setPersona(sbPersona);
        inventory.setPerformancePartsCapacity(100);
        inventory.setSkillModPartsCapacity(150);
        inventory.setVisualPartsCapacity(200);
        List<InventoryItem> items = getPowerups();
        inventory.setItems(items);
        inventoryRepository.save(inventory);
    }

    List<InventoryItem> getPowerups() {
        List<InventoryItem> inventoryItems = new ArrayList<>();
        inventoryItems.add(getPowerupInventory("nosshot", -1681514783, "0x9bc61ee1"));
        inventoryItems.add(getPowerupInventory("runflattires", -537557654, "0xdff5856a"));
        inventoryItems.add(getPowerupInventory("instantcooldown", -1692359144, "0x9b20a618"));
        inventoryItems.add(getPowerupInventory("shield", -364944936, "0xea3f61d8"));
        inventoryItems.add(getPowerupInventory("slingshot", 2236629, "0x2220d5"));
        inventoryItems.add(getPowerupInventory("ready", 957701799, "0x39155ea7"));
        inventoryItems.add(getPowerupInventory("juggernaut", 1805681994, "0x6ba0854a"));
        inventoryItems.add(getPowerupInventory("emergencyevade", -611661916, "0xdb8ac7a4"));
        inventoryItems.add(getPowerupInventory("team_emergencyevade", -1564932069, "0xa2b9081b"));
        inventoryItems.add(getPowerupInventory("onemorelap", 1627606782, "0x61034efe"));
        inventoryItems.add(getPowerupInventory("team_slingshot", 1113720384, "0x42620640"));
        inventoryItems.add(getPowerupInventory("trafficmagnet", 125509666, "0x77b2022"));
        return inventoryItems;
    }

    InventoryItem getPowerupInventory(String tag, int hash, String strHash) {
        InventoryItem inventoryItem = new InventoryItem();
        inventoryItem.setEntitlementTag(tag);
        inventoryItem.setHash(hash);
        inventoryItem.setProductId("DO NOT USE ME");
        inventoryItem.setRemainingUseCount(250);
        inventoryItem.setResalePrice(0.00F);
        inventoryItem.setStatus("ACTIVE");
        inventoryItem.setStringHash(strHash);
        inventoryItem.setVirtualItemType("powerup");
        return inventoryItem;
    }

    public Inventory getInventoryBySecurityToken(String securityToken) {
        Long activePersonaId = tokenSessionService.getActivePersonaId(securityToken);
        Optional<Inventory> inventoryByPersona = inventoryRepository.getInventoryByPersona_Id(activePersonaId);
        return inventoryByPersona.orElse(new Inventory());
    }

    public void deleteByPersona(SbPersona sbPersona) {
        inventoryRepository.deleteAllByPersona(sbPersona);
    }
}
