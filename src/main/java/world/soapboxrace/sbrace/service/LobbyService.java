package world.soapboxrace.sbrace.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import world.soapboxrace.sbrace.session.LobbySession;
import world.soapboxrace.sbrace.session.LobbySessionEntrant;
import world.soapboxrace.sbrace.session.LobbySessionService;
import world.soapboxrace.sbrace.session.TokenSessionService;
import world.soapboxrace.sbrace.xmpp.SbXmppCliServiceProxy;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LobbyService {

    private final TokenSessionService tokenSessionService;

    private final LobbySessionService lobbySessionService;

    private final SbXmppCliServiceProxy sbXmppCliServiceProxy;

    public void sendJoinEvent(String securityToken, Integer eventId) {
        Long activePersonaId = tokenSessionService.getActivePersonaId(securityToken);
        Long lobbyId = lobbySessionService.getActiveLobbyByEventId(eventId).getLobbyId();
        if (lobbyId == null) {
            lobbyId = lobbySessionService.createLobby(eventId);
        }
        sbXmppCliServiceProxy.lobbySendJoinEvent(lobbyId, eventId, activePersonaId);
    }

    public LobbySession updateEntrants(String securityToken, Long lobbyInviteId) {
        Long activePersonaId = tokenSessionService.getActivePersonaId(securityToken);
        LobbySession lobbySession = lobbySessionService.getLobbyById(lobbyInviteId);
        LobbySessionEntrant lobbySessionEntrant = new LobbySessionEntrant();
        lobbySessionEntrant.setPersonaId(activePersonaId);
        lobbySessionEntrant.setPersonaLevel(60);
        lobbySessionEntrant.setGridIndex(0);
        lobbySessionEntrant.setHeat(0F);
        lobbySessionEntrant.setState("InLobby");

        lobbySession = lobbySessionService.joinLobby(lobbySession, lobbySessionEntrant);
        List<LobbySessionEntrant> entrants = lobbySession.getEntrants();
        sendJoinMsg(entrants, lobbyInviteId, activePersonaId);
        return lobbySession;
    }

    void sendJoinMsg(List<LobbySessionEntrant> lobbyEntrants, Long lobbyId, Long fromPersonaId) {
        LobbySessionEntrant entrantToSend = lobbyEntrants.stream()
                .filter(lobbySessionEntrant -> lobbySessionEntrant.getPersonaId().equals(fromPersonaId))
                .findFirst().get();
        lobbyEntrants.stream()
                .filter(lobbySessionEntrant -> !lobbySessionEntrant.getPersonaId().equals(fromPersonaId))
                .forEach(lobbySessionEntrant -> {
                    sbXmppCliServiceProxy.sendJoinMsg(
                            entrantToSend,
                            lobbyId,
                            lobbySessionEntrant.getPersonaId()
                    );
                });
    }
}
