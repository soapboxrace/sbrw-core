package world.soapboxrace.sbrace.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import world.soapboxrace.sbrace.data.entity.SbPersona;
import world.soapboxrace.sbrace.data.repository.SbPersonaRepository;
import world.soapboxrace.sbrace.session.TokenSessionService;
import world.soapboxrace.sbrace.xmpp.SbXmppCliServiceProxy;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PersonaService {

    private final TokenSessionService tokenSessionService;
    private final SbXmppCliServiceProxy sbXmppCliServiceProxy;
    private final SbPersonaRepository sbPersonaRepository;
    private final InventoryService inventoryService;
    private final CarService carService;

    public SbPersona createPersona(String securityToken, SbPersona sbPersona) {
        sbPersona.setLevel(60);
        sbPersona.setCash(5000000);
        SbPersona persona = sbPersonaRepository.save(sbPersona);
        tokenSessionService.setActivePersonaId(securityToken, persona.getPersonaId());
        sbXmppCliServiceProxy.createAllPersonasXmpp(List.of(persona.getPersonaId()), securityToken.substring(0, 16));
        inventoryService.createInventory(persona);
        return persona;
    }

    public SbPersona getPersonaById(Long personaId) {
        return sbPersonaRepository.findById(personaId).get();
    }

    @Transactional
    public void deletePersonaById(Long personaId) {
        SbPersona sbPersona = sbPersonaRepository.findById(personaId).get();
        inventoryService.deleteByPersona(sbPersona);
        carService.deleteByPersona(sbPersona);
        sbPersonaRepository.delete(sbPersona);
    }

    public Long getActivePersonaId(String securityToken) {
        return tokenSessionService.getActivePersonaId(securityToken);
    }
}
