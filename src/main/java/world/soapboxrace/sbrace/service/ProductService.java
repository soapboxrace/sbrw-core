package world.soapboxrace.sbrace.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import world.soapboxrace.sbrace.data.entity.Product;
import world.soapboxrace.sbrace.data.entity.ProductCategory;
import world.soapboxrace.sbrace.data.repository.ProductCategoryRepository;
import world.soapboxrace.sbrace.data.repository.ProductRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    private final ProductCategoryRepository productCategoryRepository;

    public List<Product> getProductsByCategoryType(String categoryName,
                                                   String productType) {
        return productRepository.findProductByEnabledAndCategoryNameAndProductType(
                true,
                categoryName,
                productType
        );
    }

    public Product getByProductId(String productId) {
        return productRepository.findProductByProductId(productId);
    }

    public List<ProductCategory> getCategory(){
        return productCategoryRepository.findAll();
    }
}
