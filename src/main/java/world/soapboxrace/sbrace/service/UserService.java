package world.soapboxrace.sbrace.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import world.soapboxrace.sbrace.data.entity.SbPersona;
import world.soapboxrace.sbrace.data.entity.SbUser;
import world.soapboxrace.sbrace.data.repository.SbPersonaRepository;
import world.soapboxrace.sbrace.data.repository.SbUserRepository;
import world.soapboxrace.sbrace.session.TokenSessionService;
import world.soapboxrace.sbrace.session.UserSessionService;
import world.soapboxrace.sbrace.xmpp.SbXmppCliServiceProxy;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final SbXmppCliServiceProxy sbXmppCliServiceProxy;

    private final UserSessionService userSessionService;

    private final TokenSessionService tokenSessionService;

    private final SbUserRepository sbUserRepository;

    private final SbPersonaRepository sbPersonaRepository;

    public SbUser getSbUserByEmail(String email) {
        return sbUserRepository.findByEmail(email);
    }

    public String createTemporarySession(Long userId) {
        return userSessionService.createTemporarySession(userId);
    }

    public String createPermanentSession(Long userId, String securityToken) {
        return userSessionService.createPermanentSession(userId, securityToken);
    }

    public List<SbPersona> getSbPersonasByUserId(Long userId) {
        return sbPersonaRepository.findByUserId(userId);
    }

    public void createAllPersonasXmpp(List<Long> personasIds, String permanentToken) {
        sbXmppCliServiceProxy.createAllPersonasXmpp(personasIds, permanentToken.substring(0, 16));
    }

    public void setActivePersona(String securityToken, Long personaId) {
        tokenSessionService.clearActivePersona(securityToken, personaId);
        tokenSessionService.setActivePersonaId(securityToken, personaId);
    }

}
