package world.soapboxrace.sbrace.session;

import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.data.redis.core.RedisKeyExpiredEvent;
import org.springframework.stereotype.Component;
import world.soapboxrace.sbrace.config.SbrwConfig;
import world.soapboxrace.sbrace.xmpp.SbXmppCliServiceProxy;

import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.List;

@Component
@RequiredArgsConstructor
public class LobbyListener {

    private final SbXmppCliServiceProxy sbXmppCliServiceProxy;
    private final TokenSessionService tokenSessionService;
    private final SbrwConfig sbrwConfig;

    // https://github.com/SoapboxRaceWorld/soapbox-race-core/blob/develop/src/main/java/com/soapboxrace/core/bo/LobbyCountdownBO.java
    // not respecting the diamond type signature, using SpEL to fix
    @EventListener(condition = "#object.getValue() instanceof T(world.soapboxrace.sbrace.session.LobbySession)")
    public void onLobbyExpires(RedisKeyExpiredEvent<LobbySession> object) {
        LobbySession lobbySession = (LobbySession) object.getValue();
        if (lobbySession == null) {
            return;
        }
        List<LobbySessionEntrant> entrants = lobbySession.getEntrants();
        byte numOfRacers = (byte) entrants.size();
        for (LobbySessionEntrant lobbyEntrantEntity : entrants) {
            Long personaId = lobbyEntrantEntity.getPersonaId();
            byte gridIndex = lobbyEntrantEntity.getGridIndex().byteValue();
            byte[] helloPacket = {10, 11, 12, 13};
            ByteBuffer byteBuffer = ByteBuffer.allocate(48);
            byteBuffer.put(gridIndex);
            byteBuffer.put(helloPacket);
            byteBuffer.putInt(lobbySession.getLobbyId().intValue());
            byteBuffer.put(numOfRacers);
            byteBuffer.putInt(personaId.intValue());
            byte[] cryptoTicketBytes = byteBuffer.array();
            String relayCrypotTicket = Base64.getEncoder().encodeToString(cryptoTicketBytes);
            tokenSessionService.setActiveRelayCryptoTicket(personaId, relayCrypotTicket);
        }
        sbXmppCliServiceProxy.startLobbyEvent(lobbySession, sbrwConfig.getUdpRaceIp(), sbrwConfig.getUdpRacePort());
    }
}