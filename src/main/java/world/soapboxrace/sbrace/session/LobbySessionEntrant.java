package world.soapboxrace.sbrace.session;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LobbySessionEntrant {

    private Long personaId;

    private Integer personaLevel;

    private Integer gridIndex;

    private Float heat;

    private String state;

}
