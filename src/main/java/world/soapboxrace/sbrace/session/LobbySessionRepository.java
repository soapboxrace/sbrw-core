package world.soapboxrace.sbrace.session;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LobbySessionRepository extends CrudRepository<LobbySession, Long> {

    List<LobbySession> findAllByEventId(Integer eventId);

}
