package world.soapboxrace.sbrace.session;

import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
public class LobbySessionService {

    private final StringRedisTemplate stringRedisTemplate;
    private final LobbySessionRepository lobbySessionRepository;

    public LobbySession getActiveLobbyByEventId(Integer eventId) {
        List<LobbySession> allLobbiesByEventId = lobbySessionRepository.findAllByEventId(eventId);
        allLobbiesByEventId
                // something wrong with ttl annotation, need wrapper or spring bugfix
                // https://github.com/spring-projects/spring-data-redis/pull/208/files
                .forEach(lobby -> {
                            String lobbyKey = "Lobby:".concat(lobby.getLobbyId().toString());
                            Long expire = stringRedisTemplate.getExpire(lobbyKey, TimeUnit.MILLISECONDS);
                            if (expire != null) {
                                lobby.setTimeToLive(expire);
                            }
                        }
                );
        return allLobbiesByEventId.stream().filter(lobbySession -> lobbySession.getTimeToLive() > 20000L)
                .findFirst()
                .orElse(new LobbySession());
    }

    public LobbySession getLobbyById(Long lobbyId) {
        Optional<LobbySession> lobbyById = lobbySessionRepository.findById(lobbyId);
        return lobbyById.orElseGet(LobbySession::new);
    }

    public Long createLobby(Integer eventId) {
        Long nextLobbyId = getNextLobbyId();
        LobbySession lobbySession = new LobbySession(nextLobbyId, eventId, new ArrayList<>(), 60000L);
        lobbySessionRepository.save(lobbySession);
        return nextLobbyId;
    }

    public LobbySession joinLobby(LobbySession lobbySession, LobbySessionEntrant lobbySessionEntrant) {
        if (lobbySession.getEntrants() == null) {
            lobbySession.setEntrants(new ArrayList<>());
        }
        lobbySession.getEntrants().add(lobbySessionEntrant);
        return lobbySessionRepository.save(lobbySession);
    }

    private Long getNextLobbyId() {
        ValueOperations<String, String> stringStringValueOperations = stringRedisTemplate.opsForValue();
        stringStringValueOperations.increment("lobby_id");
        String lobby_id = stringStringValueOperations.get("lobby_id");
        if (lobby_id == null) {
            return 1L;
        }
        return Long.valueOf(lobby_id);
    }
}
