package world.soapboxrace.sbrace.session;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RedisHash("TokenSession")
public class TokenSession implements Serializable {

    @Id
    private String securityToken;

    private long userId;

    @Indexed
    private long activePersonaId;
    private String relayCryptoTicket;
    private long activeLobbyId;

    @TimeToLive
    private long timeToLive;
}


