package world.soapboxrace.sbrace.session;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TokenSessionRepository extends CrudRepository<TokenSession, String> {

    Optional<TokenSession> findByActivePersonaId(Long personaId);

    List<TokenSession> findAllByActivePersonaId(Long personaId);

}

