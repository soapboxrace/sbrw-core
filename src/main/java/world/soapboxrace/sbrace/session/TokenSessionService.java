package world.soapboxrace.sbrace.session;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TokenSessionService {

    private final TokenSessionRepository tokenSessionRepository;
    private final UserSessionRepository userSessionRepository;

    public void clearActivePersona(String securityToken, Long personaId) {
        List<TokenSession> allActiveSessions = tokenSessionRepository.findAllByActivePersonaId(personaId);
        if (allActiveSessions != null) {
            List<TokenSession> allExceptActive = allActiveSessions.stream().filter(tokenSession -> !securityToken.equals(tokenSession.getSecurityToken())).toList();
            tokenSessionRepository.deleteAll(allExceptActive);
        }
    }

    public Long getActivePersonaId(@PathVariable String securityToken) {
        Optional<TokenSession> tokenSessionById = tokenSessionRepository.findById(securityToken);
        if (tokenSessionById.isPresent()) {
            TokenSession tokenSession = tokenSessionById.get();
            return tokenSession.getActivePersonaId();
        }
        return 0L;
    }

    public void setActivePersonaId(@PathVariable String securityToken, @PathVariable Long personaId) {
        Optional<TokenSession> tokenSessionById = tokenSessionRepository.findById(securityToken);
        if (tokenSessionById.isPresent()) {
            TokenSession tokenSession = tokenSessionById.get();
            tokenSession.setActivePersonaId(personaId);
            tokenSessionRepository.save(tokenSession);
        }
    }

    public String getActiveRelayCryptoTicket(String securityToken) {
        Optional<TokenSession> tokenSessionById = tokenSessionRepository.findById(securityToken);
        if (tokenSessionById.isPresent()) {
            TokenSession tokenSession = tokenSessionById.get();
            return tokenSession.getRelayCryptoTicket();
        }
        return "";
    }

    public void keepAlive(@PathVariable String securityToken) {
        tokenSessionRepository.findById(securityToken).ifPresent(
                tokenSession -> {
                    tokenSession.setTimeToLive(300L);
                    tokenSessionRepository.save(tokenSession);
                    userSessionRepository.findById(tokenSession.getUserId()).ifPresent(user -> {
                        user.setTimeToLive(300L);
                        userSessionRepository.save(user);
                    });
                }
        );
    }

    public void setActiveRelayCryptoTicket(Long personaId, String relayCrypotTicket) {
        Optional<TokenSession> tokenSessionById = tokenSessionRepository.findByActivePersonaId(personaId);
        if (tokenSessionById.isPresent()) {
            TokenSession tokenSession = tokenSessionById.get();
            tokenSession.setRelayCryptoTicket(relayCrypotTicket);
            tokenSessionRepository.save(tokenSession);
        }
    }
}