package world.soapboxrace.sbrace.session;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;

import java.io.Serializable;

@RedisHash("User")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserSession implements Serializable {

    @Id
    private Long userId;
    private String token;

    @TimeToLive
    private long timeToLive;

}
