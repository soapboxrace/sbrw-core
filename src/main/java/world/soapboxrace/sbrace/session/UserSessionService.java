package world.soapboxrace.sbrace.session;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserSessionService {

    private final UserSessionRepository userRepository;
    private final TokenSessionRepository tokenSessionRepository;

    public String createTemporarySession(long userId) {
        String securityToken = UUID.randomUUID().toString();
        UserSession user = new UserSession(userId, securityToken, 15 * 60);
        userRepository.save(user);
        return securityToken;
    }

    public String createPermanentSession(long userId, String token) {
        return createPermanentSession(new UserSession(userId, token, -1)).getToken();
    }

    private UserSession createPermanentSession(UserSession user) {
        Optional<UserSession> userById = userRepository.findById(user.getUserId());
        if (userById.isPresent() && userById.get().getToken().equals(user.getToken())) {
            String securityToken = UUID.randomUUID().toString();
            user.setToken(securityToken);
            user.setTimeToLive(300L);
            tokenSessionRepository.findById(userById.get().getToken())
                    .ifPresent(tokenSessionRepository::delete);
            userRepository.save(user);
            TokenSession tokenSession = new TokenSession(
                    user.getToken(),
                    user.getUserId(),
                    0L,
                    "",
                    0L,
                    300L);
            tokenSessionRepository.save(tokenSession);
            return user;
        }
        return new UserSession();
    }

}
