package world.soapboxrace.sbrace.xmpp;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import world.soapboxrace.sbrace.session.LobbySession;
import world.soapboxrace.sbrace.session.LobbySessionEntrant;

import java.util.List;

@FeignClient(name = "sbXmppCliServiceProxy",
        path = "/sb-xmpp-cli")
public interface SbXmppCliServiceProxy {

    @PutMapping(value = "/powerups/activated/{personaId}/{targetId}/{powerupHash}/{receivers}")
    void powerupActivated(@PathVariable("personaId") Long personaId,
                          @PathVariable("targetId") Long targetId,
                          @PathVariable("powerupHash") Integer powerupHash,
                          @PathVariable("receivers") String receivers);

    @PutMapping(value = "/lobby/send-join-event/{lobbyInviteId}/{eventId}/{personaId}")
    void lobbySendJoinEvent(@PathVariable("lobbyInviteId") Long lobbyId,
                            @PathVariable("eventId") Integer eventId,
                            @PathVariable("personaId") Long personaId);

    @PutMapping(value = "/users/{password}")
    void createAllPersonasXmpp(@RequestBody List<Long> personaIds,
                               @PathVariable String password);

    @PutMapping(value = "/lobby/start-lobby-event")
    void startLobbyEvent(@RequestBody LobbySession lobbySession,
                         @RequestParam String udpRaceIp,
                         @RequestParam Integer udpRacePort);

    @PutMapping(value = "/lobby/send-join-msg")
    void sendJoinMsg(@RequestBody LobbySessionEntrant lobbySessionEntrant,
             @RequestParam Long lobbyId,
             @RequestParam Long personaId);

}
