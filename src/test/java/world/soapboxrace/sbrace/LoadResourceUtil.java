package world.soapboxrace.sbrace;

import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class LoadResourceUtil {

    static String resourceBasePath = "world/soapboxrace/sbrace/";

    public static String getFileString(String resourceFilePath) throws IOException {
        try (InputStream inputStream = new ClassPathResource(
                resourceBasePath.concat(resourceFilePath)).getInputStream()) {
            return new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
        }
    }

}
