package world.soapboxrace.sbrace.component;

import org.junit.jupiter.api.Test;
import world.soapboxrace.sbrace.controller.out.*;
import world.soapboxrace.sbrace.data.entity.*;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class CarOwnedCarDbToOutTest {

    @Test
    void copyOwnedCar() {
        CarOwnedCar carOwnedCar = new CarOwnedCar();
        carOwnedCar.setDurability(1);
        carOwnedCar.setHeat(2F);
        carOwnedCar.setId(3L);
        carOwnedCar.setOwnershipType("owner");
        OwnedCarTransOut ownedCarTransOut = CarOwnedCarDbToOut.copyOwnedCar(carOwnedCar);

        OwnedCarTransOut expected = new OwnedCarTransOut();
        expected.setDurability(1);
        expected.setHeat(2F);
        expected.setId(3L);
        expected.setOwnershipType("owner");

        assertThat(ownedCarTransOut).usingRecursiveComparison().isEqualTo(expected);
    }

    @Test
    void copyCustomCar() {
        CarCustomCar carCustomCar = new CarCustomCar();
        carCustomCar.setBaseCar(1);
        carCustomCar.setCarClassHash(2);
        carCustomCar.setIsPreset(true);
        carCustomCar.setLevel(3);
        carCustomCar.setName("name");
        carCustomCar.setPhysicsProfileHash(4);
        carCustomCar.setRating(5);
        carCustomCar.setResalePrice(6F);
        carCustomCar.setRideHeightDrop(7F);
        carCustomCar.setSkillModSlotCount(8);
        carCustomCar.setVersion(9);
        CustomCarOut customCarOut = CarOwnedCarDbToOut.copyCustomCar(carCustomCar);

        CustomCarOut expected = new CustomCarOut();
        expected.setBaseCar(1);
        expected.setCarClassHash(2);
        expected.setIsPreset(true);
        expected.setLevel(3);
        expected.setName("name");
        expected.setPhysicsProfileHash(4);
        expected.setRating(5);
        expected.setResalePrice(6F);
        expected.setRideHeightDrop(7F);
        expected.setSkillModSlotCount(8);
        expected.setVersion(9);
        expected.setPaints(new ArrayList<>());
        expected.setPerformanceParts(new ArrayList<>());
        expected.setSkillModParts(new ArrayList<>());
        expected.setVinyls(new ArrayList<>());
        expected.setVisualParts(new ArrayList<>());
        assertThat(customCarOut).usingRecursiveComparison().isEqualTo(expected);
    }

    @Test
    void copyPaints() {
        List<CarPaint> paintList = new ArrayList<>();
        CarPaint carPaint = new CarPaint();
        carPaint.setGroup(1);
        carPaint.setHue(2);
        carPaint.setSat(3);
        carPaint.setSlot(4);
        carPaint.setVar(5);
        paintList.add(carPaint);
        List<PaintsOut> paintsOuts = CarOwnedCarDbToOut.copyPaints(paintList);

        List<PaintsOut> expected = new ArrayList<>();
        PaintsOut paintsOut = new PaintsOut();
        CustomPaintTransOut customPaintTrans = new CustomPaintTransOut();
        customPaintTrans.setGroup(1);
        customPaintTrans.setHue(2);
        customPaintTrans.setSat(3);
        customPaintTrans.setSlot(4);
        customPaintTrans.setVar(5);
        paintsOut.setCustomPaintTrans(customPaintTrans);
        expected.add(paintsOut);
        assertThat(paintsOuts).usingRecursiveComparison().isEqualTo(expected);
    }

    @Test
    void copyPerformanceParts() {
        List<CarPerformancePart> performancePartList = new ArrayList<>();
        CarPerformancePart carPerformancePart = new CarPerformancePart();
        carPerformancePart.setPerformancePartAttribHash(1);
        performancePartList.add(carPerformancePart);
        List<PerformancePartsOut> performancePartsOuts = CarOwnedCarDbToOut.copyPerformanceParts(performancePartList);

        List<PerformancePartsOut> expected = new ArrayList<>();
        PerformancePartsOut performancePartsOut = new PerformancePartsOut();
        PerformancePartTransOut performancePartTrans = new PerformancePartTransOut();
        performancePartTrans.setPerformancePartAttribHash(1);
        performancePartsOut.setPerformancePartTrans(performancePartTrans);
        expected.add(performancePartsOut);
        assertThat(performancePartsOuts).usingRecursiveComparison().isEqualTo(expected);
    }

    @Test
    void copySkillModParts() {
        List<CarSkillModPart> skillModPartList = new ArrayList<>();
        CarSkillModPart carSkillModPart = new CarSkillModPart();
        carSkillModPart.setIsFixed(true);
        carSkillModPart.setSkillModPartAttribHash(1);
        skillModPartList.add(carSkillModPart);
        List<SkillModPartsOut> skillModPartsOuts = CarOwnedCarDbToOut.copySkillModParts(skillModPartList);

        List<SkillModPartsOut> expected = new ArrayList<>();
        SkillModPartsOut skillModPartsOut = new SkillModPartsOut();
        SkillModPartTransOut skillModPartTrans = new SkillModPartTransOut();
        skillModPartTrans.setIsFixed(true);
        skillModPartTrans.setSkillModPartAttribHash(1);
        skillModPartsOut.setSkillModPartTrans(skillModPartTrans);
        expected.add(skillModPartsOut);
        assertThat(skillModPartsOuts).usingRecursiveComparison().isEqualTo(expected);
    }

    @Test
    void copyVinyls() {
        List<CarVinyl> vinylList = new ArrayList<>();
        CarVinyl carVinyl = new CarVinyl();
        carVinyl.setHash(0);
        carVinyl.setHue1(1);
        carVinyl.setHue2(2);
        carVinyl.setHue3(3);
        carVinyl.setHue4(4);
        carVinyl.setLayer(5);
        carVinyl.setMir(true);
        carVinyl.setRot(6);
        carVinyl.setSat1(11);
        carVinyl.setSat2(12);
        carVinyl.setSat3(13);
        carVinyl.setSat4(14);
        carVinyl.setScaleX(15);
        carVinyl.setScaleY(16);
        carVinyl.setShear(17);
        carVinyl.setTranX(18);
        carVinyl.setTranY(19);
        carVinyl.setVar1(21);
        carVinyl.setVar2(22);
        carVinyl.setVar3(23);
        carVinyl.setVar4(24);
        vinylList.add(carVinyl);
        List<VinylsOut> vinylsOuts = CarOwnedCarDbToOut.copyVinyls(vinylList);

        List<VinylsOut> expected = new ArrayList<>();
        VinylsOut vinylsOut = new VinylsOut();
        CustomVinylTransOut customVinylTrans = new CustomVinylTransOut();
        customVinylTrans.setHash(0);
        customVinylTrans.setHue1(1);
        customVinylTrans.setHue2(2);
        customVinylTrans.setHue3(3);
        customVinylTrans.setHue4(4);
        customVinylTrans.setLayer(5);
        customVinylTrans.setMir(true);
        customVinylTrans.setRot(6);
        customVinylTrans.setSat1(11);
        customVinylTrans.setSat2(12);
        customVinylTrans.setSat3(13);
        customVinylTrans.setSat4(14);
        customVinylTrans.setScaleX(15);
        customVinylTrans.setScaleY(16);
        customVinylTrans.setShear(17);
        customVinylTrans.setTranX(18);
        customVinylTrans.setTranY(19);
        customVinylTrans.setVar1(21);
        customVinylTrans.setVar2(22);
        customVinylTrans.setVar3(23);
        customVinylTrans.setVar4(24);
        vinylsOut.setCustomVinylTrans(customVinylTrans);
        expected.add(vinylsOut);
        assertThat(vinylsOuts).usingRecursiveComparison().isEqualTo(expected);
    }

    @Test
    void copyVisualParts() {
        CarVisualPart carVisualPart = new CarVisualPart();
        carVisualPart.setPartHash(1);
        carVisualPart.setSlotHash(2);
        List<VisualPartsOut> visualPartsOuts = CarOwnedCarDbToOut.copyVisualParts(List.of(carVisualPart));

        List<VisualPartsOut> expected = new ArrayList<>();
        VisualPartsOut visualPartsOut = new VisualPartsOut();
        VisualPartTransOut visualPartTrans = new VisualPartTransOut();
        visualPartTrans.setPartHash(1);
        visualPartTrans.setSlotHash(2);
        visualPartsOut.setVisualPartTrans(visualPartTrans);
        expected.add(visualPartsOut);
        assertThat(visualPartsOuts).usingRecursiveComparison().isEqualTo(expected);
    }
}