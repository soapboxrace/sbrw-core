package world.soapboxrace.sbrace.component;

import org.junit.jupiter.api.Test;
import world.soapboxrace.sbrace.controller.out.*;
import world.soapboxrace.sbrace.controller.wrapper.PersonasDefaultcarWrapper;
import world.soapboxrace.sbrace.data.entity.*;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class OwnedCarOutToDbTest {

    @Test
    void copyOwnedCar() {
        PersonasDefaultcarWrapper ownedCarWrapper = new PersonasDefaultcarWrapper();
        OwnedCarTransOut ownedCarTrans = new OwnedCarTransOut();
        ownedCarTrans.setDurability(1);
        ownedCarTrans.setHeat(2F);
        ownedCarTrans.setId(3L);
        ownedCarTrans.setOwnershipType("owner");
        ownedCarWrapper.setOwnedCarTrans(ownedCarTrans);
        CarOwnedCar carOwnedCar = OwnedCarOutToDb.copyOwnedCar(ownedCarWrapper);

        CarOwnedCar expected = new CarOwnedCar();
        expected.setDurability(1);
        expected.setHeat(2F);
        expected.setId(3L);
        expected.setOwnershipType("owner");

        assertThat(carOwnedCar).usingRecursiveComparison().isEqualTo(expected);
    }

    @Test
    void copyCustomCar() {
        CustomCarOut customCar = new CustomCarOut();
        customCar.setBaseCar(1);
        customCar.setCarClassHash(2);
        customCar.setIsPreset(true);
        customCar.setLevel(3);
        customCar.setName("name");
        customCar.setPhysicsProfileHash(4);
        customCar.setRating(5);
        customCar.setResalePrice(6F);
        customCar.setRideHeightDrop(7F);
        customCar.setSkillModSlotCount(8);
        customCar.setVersion(9);
        CarCustomCar carCustomCar = OwnedCarOutToDb.copyCustomCar(customCar);

        CarCustomCar expected = new CarCustomCar();
        expected.setBaseCar(1);
        expected.setCarClassHash(2);
        expected.setIsPreset(true);
        expected.setLevel(3);
        expected.setName("name");
        expected.setPhysicsProfileHash(4);
        expected.setRating(5);
        expected.setResalePrice(6F);
        expected.setRideHeightDrop(7F);
        expected.setSkillModSlotCount(8);
        expected.setVersion(9);

        assertThat(carCustomCar).usingRecursiveComparison().isEqualTo(expected);
    }

    @Test
    void copyPaints() {
        List<PaintsOut> paintsList = new ArrayList<>();
        PaintsOut paintsOut = new PaintsOut();
        CustomPaintTransOut customPaintTrans = new CustomPaintTransOut();
        customPaintTrans.setGroup(1);
        customPaintTrans.setHue(2);
        customPaintTrans.setSat(3);
        customPaintTrans.setSlot(4);
        customPaintTrans.setVar(5);
        paintsOut.setCustomPaintTrans(customPaintTrans);
        paintsList.add(paintsOut);
        List<CarPaint> carPaints = OwnedCarOutToDb.copyPaints(paintsList);

        List<CarPaint> expected = new ArrayList<>();
        CarPaint carPaint = new CarPaint();
        carPaint.setGroup(1);
        carPaint.setHue(2);
        carPaint.setSat(3);
        carPaint.setSlot(4);
        carPaint.setVar(5);
        expected.add(carPaint);
        assertThat(carPaints).usingRecursiveComparison().isEqualTo(expected);
    }

    @Test
    void copyPerformanceParts() {
        List<PerformancePartsOut> performanceParts = new ArrayList<>();
        PerformancePartsOut performancePartsOut = new PerformancePartsOut();
        PerformancePartTransOut performancePartTrans = new PerformancePartTransOut();
        performancePartTrans.setPerformancePartAttribHash(1);
        performancePartsOut.setPerformancePartTrans(performancePartTrans);
        performanceParts.add(performancePartsOut);

        List<CarPerformancePart> carPerformanceParts = OwnedCarOutToDb.copyPerformanceParts(performanceParts);

        List<CarPerformancePart> expected = new ArrayList<>();
        CarPerformancePart carPerformancePart = new CarPerformancePart();
        carPerformancePart.setPerformancePartAttribHash(1);
        expected.add(carPerformancePart);
        assertThat(carPerformanceParts).usingRecursiveComparison().isEqualTo(expected);
    }

    @Test
    void copySkillModParts() {
        List<SkillModPartsOut> skillModParts = new ArrayList<>();
        SkillModPartsOut skillModPartsOut = new SkillModPartsOut();
        SkillModPartTransOut skillModPartTrans = new SkillModPartTransOut();
        skillModPartTrans.setIsFixed(true);
        skillModPartTrans.setSkillModPartAttribHash(1);
        skillModPartsOut.setSkillModPartTrans(skillModPartTrans);
        skillModParts.add(skillModPartsOut);
        List<CarSkillModPart> carSkillModParts = OwnedCarOutToDb.copySkillModParts(skillModParts);

        List<CarSkillModPart> expected = new ArrayList<>();
        CarSkillModPart carSkillModPart = new CarSkillModPart();
        carSkillModPart.setIsFixed(true);
        carSkillModPart.setSkillModPartAttribHash(1);
        expected.add(carSkillModPart);
        assertThat(carSkillModParts).usingRecursiveComparison().isEqualTo(expected);
    }

    @Test
    void copyVinyls() {
        List<VinylsOut> vinyls = getVinylsOuts();
        List<CarVinyl> carVinyls = OwnedCarOutToDb.copyVinyls(vinyls);
        List<CarVinyl> expected = getCarVinyls();
        assertThat(carVinyls).usingRecursiveComparison().isEqualTo(expected);
    }

    private static List<CarVinyl> getCarVinyls() {
        List<CarVinyl> expected = new ArrayList<>();
        CarVinyl carVinyl = new CarVinyl();
        carVinyl.setHash(0);
        carVinyl.setHue1(1);
        carVinyl.setHue2(2);
        carVinyl.setHue3(3);
        carVinyl.setHue4(4);
        carVinyl.setLayer(5);
        carVinyl.setMir(true);
        carVinyl.setRot(6);
        carVinyl.setSat1(11);
        carVinyl.setSat2(12);
        carVinyl.setSat3(13);
        carVinyl.setSat4(14);
        carVinyl.setScaleX(15);
        carVinyl.setScaleY(16);
        carVinyl.setShear(17);
        carVinyl.setTranX(18);
        carVinyl.setTranY(19);
        carVinyl.setVar1(21);
        carVinyl.setVar2(22);
        carVinyl.setVar3(23);
        carVinyl.setVar4(24);
        expected.add(carVinyl);
        return expected;
    }

    private static ArrayList<VinylsOut> getVinylsOuts() {
        ArrayList<VinylsOut> vinyls = new ArrayList<>();
        VinylsOut vinylsOut = new VinylsOut();
        CustomVinylTransOut customVinylTrans = new CustomVinylTransOut();
        customVinylTrans.setHash(0);
        customVinylTrans.setHue1(1);
        customVinylTrans.setHue2(2);
        customVinylTrans.setHue3(3);
        customVinylTrans.setHue4(4);
        customVinylTrans.setLayer(5);
        customVinylTrans.setMir(true);
        customVinylTrans.setRot(6);
        customVinylTrans.setSat1(11);
        customVinylTrans.setSat2(12);
        customVinylTrans.setSat3(13);
        customVinylTrans.setSat4(14);
        customVinylTrans.setScaleX(15);
        customVinylTrans.setScaleY(16);
        customVinylTrans.setShear(17);
        customVinylTrans.setTranX(18);
        customVinylTrans.setTranY(19);
        customVinylTrans.setVar1(21);
        customVinylTrans.setVar2(22);
        customVinylTrans.setVar3(23);
        customVinylTrans.setVar4(24);

        vinylsOut.setCustomVinylTrans(customVinylTrans);
        vinyls.add(vinylsOut);
        return vinyls;
    }

    @Test
    void copyVisualParts() {
        List<VisualPartsOut> visualParts = new ArrayList<>();
        VisualPartsOut visualPartsOut = new VisualPartsOut();
        VisualPartTransOut visualPartTrans = new VisualPartTransOut();
        visualPartTrans.setPartHash(1);
        visualPartTrans.setSlotHash(2);
        visualPartsOut.setVisualPartTrans(visualPartTrans);
        visualParts.add(visualPartsOut);

        List<CarVisualPart> carVisualParts = OwnedCarOutToDb.copyVisualParts(visualParts);

        List<CarVisualPart> expected = new ArrayList<>();
        CarVisualPart carVisualPart = new CarVisualPart();
        carVisualPart.setPartHash(1);
        carVisualPart.setSlotHash(2);
        expected.add(carVisualPart);

        assertThat(carVisualParts).usingRecursiveComparison().isEqualTo(expected);
    }

}