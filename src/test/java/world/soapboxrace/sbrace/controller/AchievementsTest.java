package world.soapboxrace.sbrace.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import world.soapboxrace.sbrace.service.PersonaService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(Achievements.class)
class AchievementsTest extends BaseControllerContext {

    @MockBean
    PersonaService personaService;

    @Test
    void loadall() throws Exception {
        Mockito.when(personaService.getActivePersonaId(Mockito.anyString()))
                .thenReturn(activePersonaId);
        String actual = mockMvc.perform(
                        get("/achievements/loadall")
                                .headers(httpHeaders)
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Achievements.lodadall.json";
        jsonAssert(expectedJsonFile, actual);
    }

}