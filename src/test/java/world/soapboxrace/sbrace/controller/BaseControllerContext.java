package world.soapboxrace.sbrace.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import world.soapboxrace.sbrace.LoadResourceUtil;
import world.soapboxrace.sbrace.component.ObjectMapperStatic;
import world.soapboxrace.sbrace.config.HttpHeaderConfig;
import world.soapboxrace.sbrace.session.TokenSessionService;

import java.io.IOException;

public abstract class BaseControllerContext {

    @MockBean
    TokenSessionService tokenSessionService;

    @Autowired
    HttpHeaderConfig httpHeaderConfig;

    @Autowired
    MockMvc mockMvc;

    ObjectMapper objectMapper = ObjectMapperStatic.get();

    HttpHeaders httpHeaders = new HttpHeaders();

    Long activePersonaId = 101L;

    @BeforeEach
    void setup() {
        httpHeaders.add("securityToken", "aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee");
        httpHeaders.add("userId", "1");
    }

    String expectedJson(String jsonFile) throws IOException {
        return LoadResourceUtil.getFileString("controller/".concat(jsonFile));
    }

    void jsonAssert(String expectedJsonFile, String actualJson) throws Exception {
        String expectedJson = expectedJson(expectedJsonFile);
        JSONAssert.assertEquals(expectedJson, actualJson, JSONCompareMode.STRICT);
    }

    String asJsonString(final Object obj) throws Exception {
        return objectMapper.writeValueAsString(obj);
    }
}
