package world.soapboxrace.sbrace.controller;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CarClasses.class)
class CarClassesTest extends BaseControllerContext {

    @Test
    void carclasses() throws Exception {
        String actual = mockMvc.perform(
                        get("/carclasses")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "CarClasses.carclasses.json";
        jsonAssert(expectedJsonFile, actual);
    }

}