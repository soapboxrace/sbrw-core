package world.soapboxrace.sbrace.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.util.LinkedMultiValueMap;
import world.soapboxrace.sbrace.data.entity.Product;
import world.soapboxrace.sbrace.data.entity.ProductCategory;
import world.soapboxrace.sbrace.service.ProductService;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(Catalog.class)
class CatalogTest extends BaseControllerContext {

    @MockBean
    ProductService productService;

    @Test
    void productsInCategoryPowerups() throws Exception {
        Product product = new Product();
        product.setCurrency("_NS");
        product.setHash(-1681514783);
        product.setLevel(0);
        product.setPrice(100F);
        product.setProductId("SRV-POWERUPX");
        Mockito.when(productService.getProductsByCategoryType(
                Mockito.anyString(),
                Mockito.anyString())).thenReturn(List.of(product));
        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("userId", "1");
        params.add("categoryName", "STORE_POWERUPS");
        params.add("clientProductType", "ANY");
        params.add("currencyType", "ANY");
        params.add("language", "EN");
        String actual = mockMvc.perform(
                        get("/catalog/productsInCategory")
                                .headers(httpHeaders).queryParams(params)
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Catalog.productsInCategory.powerups.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void productsInCategoryStartingCars() throws Exception {
        Product product = new Product();
        product.setCurrency("CASH");
        product.setDurationMinute(0);
        product.setHash(1133182666);
        product.setIcon("Black_64x64");
        product.setPrice(0F);
        product.setPriority(0);
        product.setProductId("SBRW:001");
        product.setUseCount(1);
        product.setProductTitle("BLACK");
        product.setProductType("PRESETCAR");
        Mockito.when(productService.getProductsByCategoryType(
                Mockito.anyString(),
                Mockito.anyString())).thenReturn(List.of(product));

        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("userId", "1");
        params.add("categoryName", "Starting_Cars");
        params.add("clientProductType", "ANY");
        params.add("currencyType", "ANY");
        params.add("language", "EN");
        String actual = mockMvc.perform(
                        get("/catalog/productsInCategory")
                                .headers(httpHeaders).queryParams(params)
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Catalog.productsInCategory.startingcars.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void categories() throws Exception {
        List<ProductCategory> categoryList = new ArrayList<>();
        ProductCategory category = new ProductCategory();
        category.setHash(123);
        category.setId(1L);
        category.setName("category name");
        category.setPriority(0);
        categoryList.add(category);
        List<Product> productList = new ArrayList<>();
        Product product = new Product();
        product.setHash(456);
        product.setId(2L);
        product.setPrice(500F);
        productList.add(product);
        category.setProductList(productList);
        Mockito.when(productService.getCategory()).thenReturn(categoryList);
        String actual = mockMvc.perform(
                        get("/catalog/categories")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Catalog.categories.json";
        jsonAssert(expectedJsonFile, actual);
    }
}