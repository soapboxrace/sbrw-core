package world.soapboxrace.sbrace.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import world.soapboxrace.sbrace.component.UUIDRandomBytes;
import world.soapboxrace.sbrace.service.BoostService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(Crypto.class)
class CryptoTest extends BaseControllerContext {

    @MockBean
    UUIDRandomBytes uuidRandomBytes;

    @MockBean
    BoostService boostService;

    @Test
    void cryptoticket() throws Exception {
        String actual = mockMvc.perform(
                        get("/crypto/cryptoticket")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Crypto.cryptoticket.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void relayCryptoTicket() throws Exception {
        byte[] notRandomBytes = {76, 53, 108, 112, -119, 39, 77, -43, -115, -50, 78, 94, -83, 9, -72, -27};
        Mockito.when(uuidRandomBytes.getRandomBytes()).thenReturn(notRandomBytes);
        String actual = mockMvc.perform(
                        get("/crypto/relaycryptoticket/1")
                                .headers(httpHeaders)
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Crypto.relaycryptoticket.json";
        jsonAssert(expectedJsonFile, actual);
    }
}