package world.soapboxrace.sbrace.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import world.soapboxrace.sbrace.data.entity.SbPersona;
import world.soapboxrace.sbrace.service.PersonaService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DriverPersona.class)
class DriverPersonaTest extends BaseControllerContext {

    @MockBean
    PersonaService personaService;

    @Test
    void getExpLevelPointsMap() throws Exception {
        String actual = mockMvc.perform(
                        get("/DriverPersona/GetExpLevelPointsMap")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "DriverPersona.GetExpLevelPointsMap.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void createPersona() throws Exception {
        Mockito.when(personaService.createPersona(Mockito.any(), Mockito.any())).thenReturn(new SbPersona());
        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("name", "ARROY");
        params.add("iconIndex", "0");
        String actual = mockMvc.perform(
                        post("/DriverPersona/CreatePersona")
                                .headers(httpHeaders)
                                .queryParams(params)
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "DriverPersona.CreatePersona.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void updatePersonaPresence() throws Exception {
        mockMvc.perform(
                        post("/DriverPersona/UpdatePersonaPresence")
                )
                .andExpect(status().isOk());
    }

    @Test
    void getPersonaBaseFromListAsString() throws Exception {
        Mockito.when(personaService.getPersonaById(Mockito.anyLong())).thenReturn(new SbPersona());
        String jsonString = """
                {"PersonaIdArray":{"PersonaIds":{"array:long":"101"}}}
                """;
        String actual = mockMvc.perform(
                        post("/DriverPersona/GetPersonaBaseFromList")
                                .content(jsonString)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "DriverPersona.GetPersonaBaseFromList.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void getPersonaBaseFromListAsList() throws Exception {
        Mockito.when(personaService.getPersonaById(Mockito.anyLong())).thenReturn(new SbPersona());
        String jsonString = """
                {"PersonaIdArray":{"PersonaIds":{"array:long":["101"]}}}
                """;
        String actual = mockMvc.perform(
                        post("/DriverPersona/GetPersonaBaseFromList")
                                .content(jsonString)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "DriverPersona.GetPersonaBaseFromList.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void getPersonaInfo() throws Exception {
        Mockito.when(personaService.getPersonaById(Mockito.anyLong())).thenReturn(new SbPersona());
        String actual = mockMvc.perform(
                        get("/DriverPersona/GetPersonaInfo")
                                .headers(httpHeaders)
                                .param("personaId", "101")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "DriverPersona.GetPersonaInfo.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void deletePersona() throws Exception {
        String actual = mockMvc.perform(
                        post("/DriverPersona/DeletePersona")
                                .param("personaId", "101")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "DriverPersona.DeletePersona.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void reserveName() throws Exception {
        String actual = mockMvc.perform(
                        post("/DriverPersona/ReserveName")
                                .param("name", "ARROY")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "DriverPersona.ReserveName.json";
        jsonAssert(expectedJsonFile, actual);
    }

}