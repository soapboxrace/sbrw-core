package world.soapboxrace.sbrace.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import world.soapboxrace.sbrace.controller.in.DragArbitrationPacketIn;
import world.soapboxrace.sbrace.controller.in.PursuitArbitrationPacketIn;
import world.soapboxrace.sbrace.controller.in.RouteArbitrationPacketIn;
import world.soapboxrace.sbrace.controller.in.TeamEscapeArbitrationPacketIn;
import world.soapboxrace.sbrace.controller.wrapper.EventArbitrationWrapperIn;
import world.soapboxrace.sbrace.service.PersonaService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(Event.class)
class EventTest extends BaseControllerContext {

    @MockBean
    PersonaService personaService;

    @Test
    void bust() throws Exception {
        Mockito.when(personaService.getActivePersonaId(Mockito.anyString()))
                .thenReturn(activePersonaId);
        EventArbitrationWrapperIn eventArbitrationIn = new EventArbitrationWrapperIn();
        String actual = mockMvc.perform(
                        post("/event/bust")
                                .headers(httpHeaders)
                                .content(asJsonString(eventArbitrationIn))
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("eventSessionId", "123")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Event.bust.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void arbitrationDrag() throws Exception {
        EventArbitrationWrapperIn eventArbitrationIn = new EventArbitrationWrapperIn();
        eventArbitrationIn.setDragArbitrationPacket(new DragArbitrationPacketIn());
        String actual = mockMvc.perform(
                        post("/event/arbitration")
                                .headers(httpHeaders)
                                .content(asJsonString(eventArbitrationIn))
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("eventSessionId", "123")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Event.arbitration.drag.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void arbitrationRoute() throws Exception {
        EventArbitrationWrapperIn eventArbitrationIn = new EventArbitrationWrapperIn();
        eventArbitrationIn.setRouteArbitrationPacket(new RouteArbitrationPacketIn());
        String actual = mockMvc.perform(
                        post("/event/arbitration")
                                .headers(httpHeaders)
                                .content(asJsonString(eventArbitrationIn))
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("eventSessionId", "123")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Event.arbitration.route.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void arbitrationPursuit() throws Exception {
        Mockito.when(personaService.getActivePersonaId(Mockito.anyString()))
                .thenReturn(activePersonaId);
        EventArbitrationWrapperIn eventArbitrationIn = new EventArbitrationWrapperIn();
        eventArbitrationIn.setPursuitArbitrationPacket(new PursuitArbitrationPacketIn());
        String actual = mockMvc.perform(
                        post("/event/arbitration")
                                .headers(httpHeaders)
                                .content(asJsonString(eventArbitrationIn))
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("eventSessionId", "123")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Event.arbitration.pursuit.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void arbitrationTeamScape() throws Exception {
        EventArbitrationWrapperIn eventArbitrationIn = new EventArbitrationWrapperIn();
        eventArbitrationIn.setTeamEscapeArbitrationPacket(new TeamEscapeArbitrationPacketIn());
        String actual = mockMvc.perform(
                        post("/event/arbitration")
                                .headers(httpHeaders)
                                .content(asJsonString(eventArbitrationIn))
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("eventSessionId", "123")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Event.arbitration.teamscape.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void launched() throws Exception {
        mockMvc.perform(
                        put("/event/launched")
                                .param("eventSessionId", "123")
                )
                .andExpect(status().isOk());
    }
}