package world.soapboxrace.sbrace.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import world.soapboxrace.sbrace.data.entity.Event;
import world.soapboxrace.sbrace.service.BoostService;
import world.soapboxrace.sbrace.service.EventService;

import java.util.ArrayList;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(Events.class)
class EventsTest extends BaseControllerContext {

    @MockBean
    EventService eventService;

    @MockBean
    BoostService boostService;

    @Test
    void availableatlevel() throws Exception {
        ArrayList<Event> events = new ArrayList<>();
        Event event = new Event();
        event.setId(1L);
        event.setCarClassHash(12345678);
        event.setEventModeId(2);
        events.add(event);
        Event event2 = new Event();
        event2.setId(2L);
        event2.setCarClassHash(87654321);
        event2.setEventModeId(3);
        events.add(event2);
        Mockito.when(eventService.getAllEvents()).thenReturn(events);
        String actual = mockMvc.perform(
                        get("/events/availableatlevel")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Events.availableatlevel.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void gettreasurehunteventsession() throws Exception {
        String actual = mockMvc.perform(
                        get("/events/gettreasurehunteventsession")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Events.gettreasurehunteventsession.json";
        jsonAssert(expectedJsonFile, actual);
    }
}