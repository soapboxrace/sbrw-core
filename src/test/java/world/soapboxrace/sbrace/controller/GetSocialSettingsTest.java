package world.soapboxrace.sbrace.controller;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(GetSocialSettings.class)
class GetSocialSettingsTest extends BaseControllerContext {

    @Test
    void getsocialsettings() throws Exception {
        String actual = mockMvc.perform(
                        get("/getsocialsettings")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "GetSocialSettings.json";
        jsonAssert(expectedJsonFile, actual);
    }

}