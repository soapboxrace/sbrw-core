package world.soapboxrace.sbrace.controller;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(GetUserSettings.class)
class GetUserSettingsTest extends BaseControllerContext {

    @Test
    void getUserSettings() throws Exception {
        String actual = mockMvc.perform(
                        get("/getusersettings")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "GetUserSettings.json";
        jsonAssert(expectedJsonFile, actual);
    }
}