package world.soapboxrace.sbrace.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import world.soapboxrace.sbrace.service.LobbyService;
import world.soapboxrace.sbrace.session.LobbySession;
import world.soapboxrace.sbrace.session.LobbySessionEntrant;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(Matchmaking.class)
class MatchmakingTest extends BaseControllerContext {

    @MockBean
    LobbyService lobbyService;

    @Test
    void joinqueueevent() throws Exception {
        mockMvc.perform(
                        put("/matchmaking/joinqueueevent/123")
                                .headers(httpHeaders)
                )
                .andExpect(status().isOk());
    }

    @Test
    void acceptinvite() throws Exception {
        LobbySession lobbySession = getLobbySession();
        Mockito.when(lobbyService.updateEntrants(Mockito.anyString(), Mockito.anyLong()))
                .thenReturn(lobbySession);
        String actual = mockMvc.perform(
                        put("/matchmaking/acceptinvite")
                                .headers(httpHeaders)
                                .param("lobbyInviteId", "123")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Matchmaking.acceptinvite.json";
        jsonAssert(expectedJsonFile, actual);
    }

    LobbySession getLobbySession() {
        List<LobbySessionEntrant> lobbySessionEntrantList = new ArrayList<>();
        LobbySession lobbySession = new LobbySession();
        LobbySessionEntrant lobbySessionEntrant1 = new LobbySessionEntrant();
        lobbySessionEntrant1.setPersonaId(101L);
        lobbySessionEntrant1.setHeat(0F);
        lobbySessionEntrant1.setState("InLobby");
        lobbySessionEntrant1.setGridIndex(0);
        lobbySessionEntrantList.add(lobbySessionEntrant1);
        LobbySessionEntrant lobbySessionEntrant2 = new LobbySessionEntrant();
        lobbySessionEntrant2.setPersonaId(102L);
        lobbySessionEntrant2.setHeat(0F);
        lobbySessionEntrant2.setState("InLobby");
        lobbySessionEntrant2.setGridIndex(0);
        lobbySessionEntrantList.add(lobbySessionEntrant2);
        lobbySession.setEntrants(lobbySessionEntrantList);
        return lobbySession;
    }

    @Test
    void leavelobby() throws Exception {
        mockMvc.perform(
                        put("/matchmaking/leavelobby")
                                .headers(httpHeaders)
                )
                .andExpect(status().isOk());
    }

    @Test
    void launchEvent() throws Exception {
        String actual = mockMvc.perform(
                        get("/matchmaking/launchevent/123")
                                .headers(httpHeaders)
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Matchmaking.launchevent.eventId.json";
        jsonAssert(expectedJsonFile, actual);
    }
}