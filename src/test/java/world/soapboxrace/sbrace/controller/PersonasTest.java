package world.soapboxrace.sbrace.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import world.soapboxrace.sbrace.LoadResourceUtil;
import world.soapboxrace.sbrace.controller.in.BasketItemTransIn;
import world.soapboxrace.sbrace.controller.in.BasketItensIn;
import world.soapboxrace.sbrace.controller.in.BasketTransIn;
import world.soapboxrace.sbrace.controller.wrapper.PersonasBasketsWrapperIn;
import world.soapboxrace.sbrace.data.entity.*;
import world.soapboxrace.sbrace.service.BoostService;
import world.soapboxrace.sbrace.service.CarService;
import world.soapboxrace.sbrace.service.InventoryService;
import world.soapboxrace.sbrace.service.ProductService;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(Personas.class)
class PersonasTest extends BaseControllerContext {

    @MockBean
    ProductService productService;

    @MockBean
    CarService carService;

    @MockBean
    InventoryService inventoryService;

    @MockBean
    BoostService boostService;

    @Test
    void carslots() throws Exception {
        List<CarOwnedCar> carOwnedByPersonaList = new ArrayList<>();
        CarOwnedCar carOwnedCar = new CarOwnedCar();
        carOwnedByPersonaList.add(carOwnedCar);
        carOwnedByPersonaList.add(carOwnedCar);
        Mockito.when(carService.getPersonaCars(Mockito.anyLong()))
                .thenReturn(carOwnedByPersonaList);
        String actual = mockMvc.perform(
                        get("/personas/101/carslots")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Personas.personaId.carslots.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void inventoryObjects() throws Exception {
        Inventory inventory = new Inventory();
        inventory.getItems().add(new InventoryItem());
        inventory.getItems().add(new InventoryItem());
        Mockito.when(inventoryService.getInventoryBySecurityToken(Mockito.anyString()))
                .thenReturn(inventory);
        String actual = mockMvc.perform(
                        get("/personas/inventory/objects")
                                .headers(httpHeaders)
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Personas.inventory.objects.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void putDefaultCar() throws Exception {
        mockMvc.perform(
                        put("/personas/101/defaultcar/123")
                                .headers(httpHeaders)
                )
                .andExpect(status().isOk());
    }

    @Test
    void sellCar() throws Exception {
        mockMvc.perform(
                        post("/personas/101/cars")
                                .param("serialNumber", "1")
                )
                .andExpect(status().isOk());
    }

    @Test
    void cars() throws Exception {
        List<CarOwnedCar> carOwnedByPersonaList = new ArrayList<>();
        CarOwnedCar carOwnedCar = new CarOwnedCar();
        carOwnedByPersonaList.add(carOwnedCar);
        carOwnedByPersonaList.add(carOwnedCar);
        Mockito.when(carService.getPersonaCars(Mockito.anyLong()))
                .thenReturn(carOwnedByPersonaList);
        String actual = mockMvc.perform(
                        get("/personas/101/cars")
                                .param("serialNumber", "1")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Personas.personaId.cars.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void baskets() throws Exception {
        Product product = new Product();
        product.setProductType("PRESETCAR");
        Mockito.when(productService.getByProductId(Mockito.anyString()))
                .thenReturn(product);
        PersonasBasketsWrapperIn personasBasketsIn = new PersonasBasketsWrapperIn();
        BasketTransIn basketTrans = new BasketTransIn();
        BasketItensIn items = new BasketItensIn();
        BasketItemTransIn basketItemTrans = new BasketItemTransIn();
        basketItemTrans.setProductId("SOME_CAR");
        items.setBasketItemTrans(basketItemTrans);
        basketTrans.setItems(items);
        personasBasketsIn.setBasketTrans(basketTrans);
        String actual = mockMvc.perform(
                        post("/personas/101/baskets")
                                .headers(httpHeaders)
                                .content(asJsonString(personasBasketsIn))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Personas.personaId.baskets.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void getDefaultCar() throws Exception {
        CarOwnedCar carOwnedCar = new CarOwnedCar();
        carOwnedCar.setDurability(1);
        carOwnedCar.setHeat(2F);
        carOwnedCar.setId(3L);
        CarCustomCar customCar = new CarCustomCar();
        List<CarSkillModPart> skillModParts = new ArrayList<>();
        CarSkillModPart carSkillModPart1 = new CarSkillModPart();
        carSkillModPart1.setIsFixed(true);
        carSkillModPart1.setSkillModPartAttribHash(123);
        skillModParts.add(carSkillModPart1);
        CarSkillModPart carSkillModPart2 = new CarSkillModPart();
        carSkillModPart2.setIsFixed(false);
        carSkillModPart2.setSkillModPartAttribHash(456);
        skillModParts.add(carSkillModPart2);
        customCar.setSkillModParts(skillModParts);
        carOwnedCar.setCustomCar(customCar);
        carOwnedCar.setCustomCar(customCar);
        Mockito.when(carService.getPersonaDefaultCar(Mockito.anyLong()))
                .thenReturn(carOwnedCar);
        String actual = mockMvc.perform(
                        get("/personas/101/defaultcar")
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "Personas.personaId.defaultcar.json";
        System.out.println(actual);
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void postCustomization() throws Exception {
        String jsonIn = LoadResourceUtil.getFileString("controller/Personas.personaId.commerce.json");
        String actual = mockMvc.perform(
                        post("/personas/101/commerce")
                                .headers(httpHeaders)
                                .content(jsonIn)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        System.out.println(actual);
//        String expectedJsonFile = "Personas.personaId.baskets.json";
//        jsonAssert(expectedJsonFile, actual);
    }
}