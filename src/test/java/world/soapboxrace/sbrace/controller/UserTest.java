package world.soapboxrace.sbrace.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.util.LinkedMultiValueMap;
import world.soapboxrace.sbrace.data.entity.SbPersona;
import world.soapboxrace.sbrace.data.entity.SbUser;
import world.soapboxrace.sbrace.service.UserService;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(User.class)
class UserTest extends BaseControllerContext {

    @MockBean
    UserService userService;

    @Test
    void authenticateUser() throws Exception {
        SbUser sbUser = new SbUser();
        sbUser.setId(1L);
        sbUser.setPassword("123");
        Mockito.when(userService.getSbUserByEmail(Mockito.anyString())).thenReturn(sbUser);
        String token = "token";
        Mockito.when(userService.createTemporarySession(Mockito.anyLong())).thenReturn(token);
        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("email", "debug@local");
        params.add("password", "123");
        String actual = mockMvc.perform(
                        get("/User/authenticateUser")
                                .params(params)
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        System.out.println(actual);
        String expectedJsonFile = "User.authenticateUser.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void getPermanentSession() throws Exception {
        SbPersona sbPersona = new SbPersona();
        sbPersona.setPersonaId(101L);
        Mockito.when(userService.getSbPersonasByUserId(Mockito.anyLong()))
                .thenReturn(List.of(sbPersona));
        String actual = mockMvc.perform(
                        post("/User/GetPermanentSession")
                                .headers(httpHeaders)
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String expectedJsonFile = "User.GetPermanentSession.json";
        jsonAssert(expectedJsonFile, actual);
    }

    @Test
    void secureLoginPersona() throws Exception {
        mockMvc.perform(
                        post("/User/SecureLoginPersona")
                                .headers(httpHeaders)
                                .param("personaId", "101")
                )
                .andExpect(status().isOk());
    }

    @Test
    void secureLogout() throws Exception {
        mockMvc.perform(
                        post("/User/SecureLogout")
                )
                .andExpect(status().isOk());
    }

    @Test
    void secureLogoutPersona() throws Exception {
        mockMvc.perform(
                        post("/User/SecureLogoutPersona")
                )
                .andExpect(status().isOk());
    }
}