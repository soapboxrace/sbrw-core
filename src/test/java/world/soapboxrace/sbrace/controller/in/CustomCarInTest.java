package world.soapboxrace.sbrace.controller.in;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import world.soapboxrace.sbrace.LoadResourceUtil;
import world.soapboxrace.sbrace.component.ObjectMapperStatic;

class CustomCarInTest {

    @Test
    void deserialize() throws Exception {
        String json = LoadResourceUtil.getFileString("controller/in/CustomCarIn.json");
        ObjectMapper objectMapper = ObjectMapperStatic.get();
        CustomCarIn customCarIn = objectMapper.readValue(json, CustomCarIn.class);
        System.out.println(customCarIn);
    }

}