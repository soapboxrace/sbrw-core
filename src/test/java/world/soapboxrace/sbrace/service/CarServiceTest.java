package world.soapboxrace.sbrace.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import world.soapboxrace.sbrace.component.ObjectMapperStatic;
import world.soapboxrace.sbrace.data.entity.*;
import world.soapboxrace.sbrace.data.repository.CarBasketRepository;
import world.soapboxrace.sbrace.data.repository.CarOwnedCarRepository;
import world.soapboxrace.sbrace.data.repository.SbPersonaRepository;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class CarServiceTest {

    static ObjectMapper objectMapper = ObjectMapperStatic.get();

    @Mock
    SbPersonaRepository sbPersonaRepository;

    @Mock
    CarBasketRepository carBasketRepository;

    @Mock
    CarOwnedCarRepository carOwnedCarRepository;

    @Captor
    ArgumentCaptor<CarOwnedCar> carOwnedCarArgumentCaptor;

    @InjectMocks
    private CarService carService;

    @ParameterizedTest
    @CsvFileSource(
            resources = "/world/soapboxrace/sbrace/service/car_basket.csv",
            numLinesToSkip = 1,
            maxCharsPerColumn = 11000)
    void buyCar(String productId, String ownedCarJson) throws Exception {
        SbPersona sbPersona = new SbPersona();
        Mockito.when(sbPersonaRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(sbPersona));
        CarBasket carBasket = new CarBasket();
        carBasket.setJson(ownedCarJson);
        Mockito.when(carBasketRepository.findById(productId))
                .thenReturn(Optional.of(carBasket));
        Product product = new Product();
        product.setProductId(productId);
        carService.buyCar(101L, product);
        Mockito.verify(carOwnedCarRepository).save(carOwnedCarArgumentCaptor.capture());
        CarOwnedCar carOwnedCarToSave = carOwnedCarArgumentCaptor.getValue();

        CarOwnedCar expected = objectMapper.readValue(ownedCarJson, CarOwnedCar.class);
        expected.setDefaultCar(true);
        String customCarJson = objectMapper.writeValueAsString(expected.getCustomCar());
        expected.setCustomCarJson(customCarJson);
        expected.setCustomCar(null);
        expected.setPersona(sbPersona);
        assertThat(carOwnedCarToSave).usingRecursiveComparison().isEqualTo(expected);
    }

    @Test
    void buyCarJsonException() {
        CarBasket carBasket = new CarBasket();
        carBasket.setJson("");
        Mockito.when(carBasketRepository.findById(Mockito.anyString()))
                .thenReturn(Optional.of(carBasket));
        Product product = new Product();
        product.setProductId("CAR");
        Assertions.assertThatThrownBy(() -> carService.buyCar(101L, product));
    }

    @Test
    void getPersonaDefaultCar() {
        SbPersona sbPersona = new SbPersona();
        Mockito.when(sbPersonaRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(sbPersona));
        CarOwnedCar carOwnedCar = new CarOwnedCar();
        carOwnedCar.setCustomCarJson("{}");
        Mockito.when(carOwnedCarRepository.getCarOwnedCarsByPersonaOrderByDefaultCarDesc(Mockito.any()))
                .thenReturn(List.of(carOwnedCar));
        CarOwnedCar actual = carService.getPersonaDefaultCar(101L);

        CarOwnedCar expected = new CarOwnedCar();
        expected.setCustomCar(new CarCustomCar());
        assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
    }

}