package world.soapboxrace.sbrace.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import world.soapboxrace.sbrace.session.LobbySessionEntrant;
import world.soapboxrace.sbrace.xmpp.SbXmppCliServiceProxy;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class LobbyServiceTest {

    @Mock
    SbXmppCliServiceProxy sbXmppCliServiceProxy;

    @Captor
    ArgumentCaptor<LobbySessionEntrant> entrantToSendArgumentCaptor;

    @InjectMocks
    LobbyService lobbyService;

    @Test
    void sendJoinEvent() {
    }

    @Test
    void updateEntrants() {
    }

    @Test
    void sendJoinMsg() {
        LobbySessionEntrant lobbySessionEntrant1 = new LobbySessionEntrant();
        lobbySessionEntrant1.setPersonaId(101L);
        LobbySessionEntrant lobbySessionEntrant2 = new LobbySessionEntrant();
        lobbySessionEntrant2.setPersonaId(102L);
        List<LobbySessionEntrant> lobbyEntrants = new ArrayList<>();
        lobbyEntrants.add(lobbySessionEntrant1);
        lobbyEntrants.add(lobbySessionEntrant2);
        lobbyService.sendJoinMsg(lobbyEntrants, 1L, 101L);
        Mockito.verify(sbXmppCliServiceProxy).sendJoinMsg(
                entrantToSendArgumentCaptor.capture(),
                Mockito.anyLong(),
                Mockito.anyLong());
        LobbySessionEntrant entrantToSend = entrantToSendArgumentCaptor.getValue();

        assertThat(entrantToSend).usingRecursiveComparison().isEqualTo(lobbySessionEntrant1);
    }
}